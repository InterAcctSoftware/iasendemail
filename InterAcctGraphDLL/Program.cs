﻿
using Console_Interactive_CustomWebUI.CustomWebBrowser;
using Microsoft.Extensions.Configuration;
using Microsoft.Graph;
using Microsoft.Identity.Client.Extensibility;
using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Security.Cryptography;
using Microsoft.Identity.Client;
using System.Text.RegularExpressions;
using Microsoft.Graph.SecurityNamespace;
using System.Security.Policy;

namespace InterAcctGraphDLL
{

    

    static class TokenCacheHelper
    {
        /// <summary>
        /// Path to the token cache
        /// </summary>
        /// 
        public static Regex rgx = new("[^a-zA-Z0-9 -]");
        //public static string CacheFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location + ".msalcache.bin3";
        //public static string CacheFilePath = System.Reflection.Assembly.GetExecutingAssembly().l + "." + rgx.Replace(Environment.UserName.ToString(), "") + ".msalcache.bin3";
        public static string CacheFilePath = AppDomain.CurrentDomain.BaseDirectory + "\\Cache\\" + rgx.Replace(Environment.UserName.ToString(), "") + ".msalcache.bin3";
        public static string CacheFilePathOnly = AppDomain.CurrentDomain.BaseDirectory + "\\Cache\\";

        private static readonly object FileLock = new();

        public static void BeforeAccessNotification(TokenCacheNotificationArgs args)
        {

            //Remove all Full Stops and other characters so we just have AlphaNumeric
            
            //string LoggedInEmailAlphaOnly = rgx.Replace(args.Account.Username.ToString(), "");
            //string LoggedInEmailAlphaOnly = rgx.Replace(Environment.UserName.ToString(), "");
            //CacheFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location + LoggedInEmailAlphaOnly + ".msalcache.bin3";

            lock (FileLock)
            {
                try
                {
                    args.TokenCache.DeserializeMsalV3(System.IO.File.Exists(CacheFilePath)
                        ? ProtectedData.Unprotect(System.IO.File.ReadAllBytes(CacheFilePath),
                                                 null,
                                                 DataProtectionScope.CurrentUser)
                        : null);
                }
                catch (Exception ex)
                {

                    
                    DialogResult dialogResult = MessageBox.Show(ex.Message + Environment.NewLine + Environment.NewLine + "Do you want to recreate the Certificate Token?" , "MsGraph Token Incorrect Certificate Token", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        //do something
                        System.IO.File.Delete(CacheFilePath);
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        //do something else
                        
                    }

                }

            }
        }

        public static void AfterAccessNotification(TokenCacheNotificationArgs args)
        {


            //Remove all Full Stops and other characters so we just have AlphaNumeric
            //Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            //string LoggedInEmailAlphaOnly = rgx.Replace(args.Account.Username.ToString(), "");
            //string LoggedInEmailAlphaOnly = rgx.Replace(Environment.UserName.ToString(), "");
            //CacheFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location + LoggedInEmailAlphaOnly + ".msalcache.bin3";

            if (args.HasStateChanged)
            {
                lock (FileLock)
                {
                    // reflect changesgs in the persistent store
                    System.IO.File.WriteAllBytes(CacheFilePath,
                                       ProtectedData.Protect(args.TokenCache.SerializeMsalV3(),
                                                             null,
                                                             DataProtectionScope.CurrentUser)
                                      );
                }
            }
        }

        public static void DeleteTokenFile()
        {
            System.IO.File.Delete(CacheFilePath);
        }

        internal static void EnableSerialization(ITokenCache tokenCache)
        {

            System.IO.Directory.CreateDirectory(CacheFilePathOnly);

            tokenCache.SetBeforeAccess(BeforeAccessNotification);
            tokenCache.SetAfterAccess(AfterAccessNotification);
        }
    }




    public class Program
    {
        private static PublicClientApplicationOptions appConfiguration = null;
        private static IConfiguration configuration;
        private static string graphURL;

        private static string msgraphToken;

        public static bool errorOccured = false;

        public static bool gBusyStatus = false;
        public static bool gFileUploaded = false;
        public static string gUploadStatus;
        public static eBusyStatus gStatusName { get; set; }

        public enum eBusyStatus
        {
            signIn,
            setupFolder,
            sendingFile,
            sendingEmail,
            error,
        }

        //private static IAccount _currentUserAccount;

        // Since the browser is started via Process.Start, there is no control over it,
        // So it is recommended to configure a timeout 
        // private const int TimeoutWaitingForBrowserMs = 30 * 1000; //30 seconds

        public struct EmailType
        {
            public EmailType(string emailReceipt, string emailFullName, int emailSend)
            {
                EmailReceipt = emailReceipt;
                EmailFullName = emailFullName;
                EmailSend = emailSend;
            }

            public string EmailReceipt { get; }
            public string EmailFullName { get; }
            public int EmailSend { get; }
        }





        private static string GetSettingsFolder()
        {
            string settingsFolder = Path.Combine(System.IO.Directory.GetCurrentDirectory());
            string databaseFolder = settingsFolder;

            if (!System.IO.File.Exists(settingsFolder + "\\appsettings.json"))
            {
                settingsFolder = Path.Combine(System.Windows.Forms.Application.StartupPath);

                if (!System.IO.File.Exists(settingsFolder + "\\appsettings.json"))
                {
                    MessageBox.Show("Unable to detect a valid appsettings.json file" + Environment.NewLine + Environment.NewLine +
                        "Add to either:" + Environment.NewLine +
                        "App Path " + System.Windows.Forms.Application.StartupPath + Environment.NewLine +
                        "Database Folder " + databaseFolder, "IASharePoint - No appsettings.json file", MessageBoxButtons.OK);
                    return "";
                }
            }
            return settingsFolder;
        }



        public static async Task MakeFolder(GraphServiceClient graphClient, IPublicClientApplication application, string siteID, string selectedDriveID, string targetFolder)
        {

            //Check for the best appsettings Folder

            gUploadStatus = "Making Folder on SharePoint " + targetFolder;
            gStatusName = eBusyStatus.setupFolder;

            string baseFolder = GetSettingsFolder();
            if (baseFolder.Length > 0)
            {

                // Using appsettings.json as our configuration settings
                var builder = new ConfigurationBuilder()
                .SetBasePath(baseFolder)
                .AddJsonFile("appsettings.json");

                configuration = builder.Build();

                appConfiguration = configuration
                    .Get<PublicClientApplicationOptions>();


                string[] scopes = new[] { "Files.ReadWrite.All", "Sites.ReadWrite.All", "Sites.Manage.All" };

                //graphURL = configuration.GetValue<string>("GraphApiUrl") + "me/sendMail";
                graphURL = configuration.GetValue<string>("GraphApiUrl");

                //MessageBox.Show("Prior to SignInAndInitializeGraphServiceClient", "IASharePoint Message", MessageBoxButtons.OK);

                // Sign-in user using MSAL and obtain an access token for MS Graph
                graphClient = await SignInAndInitializeGraphServiceClient(application, appConfiguration, scopes, "");


                //MessageBox.Show("Prior to CallMSGraphGetDrives", "IASharePoint Message", MessageBoxButtons.OK);

                await CallMSGraphMakeFolder(graphClient, siteID, selectedDriveID, targetFolder);



            }
            return;


        }


        public static async Task MakeDrive(GraphServiceClient graphClient, IPublicClientApplication application, string siteID, string driveName)
        {

            //Check for the best appsettings Folder
            gUploadStatus = "Making Drive on SharePoint " + driveName;
            gStatusName = eBusyStatus.setupFolder;

            string baseFolder = GetSettingsFolder();
            if (baseFolder.Length > 0)
            {

                // Using appsettings.json as our configuration settings
                var builder = new ConfigurationBuilder()
                .SetBasePath(baseFolder)
                .AddJsonFile("appsettings.json");

                configuration = builder.Build();

                appConfiguration = configuration
                    .Get<PublicClientApplicationOptions>();





                string[] scopes = new[] { "files.readwrite.all", "sites.readwrite.all", "sites.manage.all" };

                //graphURL = configuration.GetValue<string>("GraphApiUrl") + "me/sendMail";
                graphURL = configuration.GetValue<string>("GraphApiUrl");

                //MessageBox.Show("Prior to SignInAndInitializeGraphServiceClient", "IASharePoint Message", MessageBoxButtons.OK);

                // Sign-in user using MSAL and obtain an access token for MS Graph
                graphClient = await SignInAndInitializeGraphServiceClient(application, appConfiguration, scopes, "");


                //MessageBox.Show("Prior to CallMSGraphGetDrives", "IASharePoint Message", MessageBoxButtons.OK);

                await CallMSGraphMakeDrive(graphClient, siteID, driveName);

            }
            return;


        }


        public static async Task<string> MainGetDrives(GraphServiceClient graphClient, IPublicClientApplication application, string siteID)
        {

            //Check for the best appsettings Folder
            gUploadStatus = "Getting Drives on SharePoint. SiteID: " + siteID;
            gStatusName = eBusyStatus.setupFolder;


            string baseFolder = GetSettingsFolder();
            if (baseFolder.Length > 0)
            {

                // Using appsettings.json as our configuration settings
                var builder = new ConfigurationBuilder()
                .SetBasePath(baseFolder)
                .AddJsonFile("appsettings.json");

                configuration = builder.Build();

                appConfiguration = configuration
                    .Get<PublicClientApplicationOptions>();





                string[] scopes = new[] { "files.readwrite.all", "sites.readwrite.all", "sites.manage.all" };

                //graphURL = configuration.GetValue<string>("GraphApiUrl") + "me/sendMail";
                graphURL = configuration.GetValue<string>("GraphApiUrl");

                //MessageBox.Show("Prior to SignInAndInitializeGraphServiceClient", "IASharePoint Message", MessageBoxButtons.OK);

                // Sign-in user using MSAL and obtain an access token for MS Graph
                graphClient = await SignInAndInitializeGraphServiceClient(application, appConfiguration, scopes, "");


                //MessageBox.Show("Prior to CallMSGraphGetDrives", "IASharePoint Message", MessageBoxButtons.OK);

                var result = await CallMSGraphGetDrives(graphClient, siteID);

                return result;

            }
            return null;


        }

        public static void AddToMailLogFile(string Area, string Title, string Status, string Description, string emailSubject, List<Microsoft.Graph.Recipient> ToRecipientList, List<Microsoft.Graph.Recipient> CcRecipientList
            , Microsoft.Graph.Recipient FromEmail
            , Microsoft.Graph.MessageAttachmentsCollectionPage attachments
            , bool SaveToDraft
            )
        {

            string ToAddress = "";
            string CcAddress = "";
            string FromAddress = "";
            string AttachementNames = "";



            if (ToRecipientList.Count > 0)
            {
                ToAddress = ToRecipientList.FirstOrDefault().EmailAddress.Address;
            }

//            if (CcRecipientList.Count > 0)
//            {
//                CcAddress = CcRecipientList.FirstOrDefault().EmailAddress.Address;
//            }

            if (FromEmail != null)
            {
                if (FromEmail.EmailAddress != null)
                {
                    FromAddress = FromEmail.EmailAddress.Address;
                }
            }

            if (attachments.Count > 0)
            {
                AttachementNames = attachments.FirstOrDefault().Name;
            }



            AddToLogFile(Area, Title, Status, Description, emailSubject, ToAddress, CcAddress, FromAddress, AttachementNames, SaveToDraft);

        }


            public static void AddToLogFile(string Area, string Title, string Status, string Description, string filePath, string fileName, string siteID, string targetFolder, string selectedDriveID, bool deleteAfterSaving)
        {
            //PWR 23.03.2023 Creating Logging Function
            string LogFilePath = AppDomain.CurrentDomain.BaseDirectory + "\\LOG";
            string LogFileName = LogFilePath+ "\\" + Area + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";

            if (!System.IO.Directory.Exists(LogFilePath))
            {
                System.IO.Directory.CreateDirectory(LogFilePath);
                
            }


            if (!System.IO.File.Exists(LogFileName))
            {

                string LogHeaderString = "Date,Time,UserName,Area,Status,Title,Description,filePath,fileName,siteID,targetFolder,selectedDriveID,deleteAfterSaving";
                if (Area != "IASharePoint")
                {
                    LogHeaderString = "Date,Time,UserName,Area,Status,Title,Description,emailSubject,ToRecipientList,CcRecipientList,FromEmail,attachments,SaveToDraft";
                }
                System.IO.File.AppendAllText(LogFileName, LogHeaderString + Environment.NewLine);
            }


                string LogString = DateTime.Now.ToString("yyyy-MM-dd") + "," + DateTime.Now.ToString("HH:mm:ss") + "," + Environment.UserName.ToString() + "," + Area + "," + Status + "," + Title + "," + Description + "," + filePath + "," + fileName + ",\"" + siteID + "\"," + targetFolder + "," + selectedDriveID + "," + deleteAfterSaving;
            System.IO.File.AppendAllText(LogFileName, LogString + Environment.NewLine);

        }



        public static async Task MainSharePointSave(GraphServiceClient graphClient, IPublicClientApplication application, string filePath, string fileName, string siteID, string targetFolder, string selectedDriveID, bool deleteAfterSaving)
        {

            gUploadStatus = "Saving the File to SharePoint Start. filePath: " + filePath;
            gStatusName = eBusyStatus.sendingFile;

            AddToLogFile("IASharePoint", "MainSharePointSave", "Start", "Saving the File to SharePoint Start", filePath, fileName, siteID, targetFolder, selectedDriveID, deleteAfterSaving);

            string baseFolder = GetSettingsFolder();
            if (baseFolder.Length > 0)
            {


                // Using appsettings.json as our configuration settings
                var builder = new ConfigurationBuilder()
                    .SetBasePath(baseFolder)
                    .AddJsonFile("appsettings.json");

                configuration = builder.Build();

                appConfiguration = configuration
                    .Get<PublicClientApplicationOptions>();


                string[] scopes = new[] { "Files.ReadWrite.All", "Sites.ReadWrite.All" };

                //graphURL = configuration.GetValue<string>("GraphApiUrl") + "me/sendMail";
                graphURL = configuration.GetValue<string>("GraphApiUrl");

                //MessageBox.Show("Prior to SignInAndInitializeGraphServiceClient", "IASharePoint Message", MessageBoxButtons.OK);

                // Sign-in user using MSAL and obtain an access token for MS Graph
                graphClient = await SignInAndInitializeGraphServiceClient(application, appConfiguration, scopes, "");


                //MessageBox.Show("Prior to CallMSGraphSendSharePoint", "IASharePoint Message", MessageBoxButtons.OK);

                CallMSGraphSendSharePoint(graphClient, filePath, fileName, siteID, targetFolder, selectedDriveID, deleteAfterSaving);

            }




        }




        public async Task MainSendEmail(GraphServiceClient graphClient, IPublicClientApplication application,
            string emailSubject, string emailBody
            , List<Microsoft.Graph.Recipient> ToRecipientList
            , List<Microsoft.Graph.Recipient> CcRecipientList
            , List<Microsoft.Graph.Recipient> BccRecipientList
            , Microsoft.Graph.Recipient FromEmail
            , string tokenToUse
            , Microsoft.Graph.MessageAttachmentsCollectionPage attachments
            , string EmailBodyHTML
            , bool SaveToDraft
            , bool OpenEmailForm
            )


        {
            //MessageBox.Show("Very Start", "IASendEmail - MainSendEmail", MessageBoxButtons.OK);


            //var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            //MessageBox.Show("Current Directory : " + currentDirectory, "IASendEmail - MainSendEmail - Current Directory", MessageBoxButtons.OK);

            // Using appsettings.json as our configuration settings

            string baseFolder = GetSettingsFolder();
            if (baseFolder.Length > 0)
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(baseFolder)
                .AddJsonFile("appsettings.json");

                //MessageBox.Show("Before Builder Build", "IASendEmail - MainSendEmail", MessageBoxButtons.OK);
                try
                {
                    configuration = builder.Build();
                }
                catch (Exception ex)
                {
                    gUploadStatus = "IASharePoint Error Message - No File in Directory";
                    gStatusName = eBusyStatus.error;
                    MessageBox.Show(ex.Message, "IASharePoint Error Message - No File in Directory", MessageBoxButtons.OK);
                    errorOccured = true;
                }


                //MessageBox.Show("Before AppConfig", "IASendEmail - MainSendEmail", MessageBoxButtons.OK);

                appConfiguration = configuration
                    .Get<PublicClientApplicationOptions>();

                //string[] scopes = new[] { "user.read" };

                string[] scopes = new[] { "mail.send" };

                //MessageBox.Show("Before configuration GetValue", "IASendEmail - MainSendEmail", MessageBoxButtons.OK);

                //graphURL = configuration.GetValue<string>("GraphApiUrl") + "me/sendMail";
                graphURL = configuration.GetValue<string>("GraphApiUrl");

                //MessageBox.Show("Start of DLL Routine", "IASharePoint Start - MainSendEmail", MessageBoxButtons.OK);


                AddToMailLogFile("IASendEmail", "MainSendEmail", "Start", "Before Logging into MSGraph", emailSubject, ToRecipientList, CcRecipientList, FromEmail, attachments, SaveToDraft);


                // Sign-in user using MSAL and obtain an access token for MS Graph
                graphClient = await SignInAndInitializeGraphServiceClient(application, appConfiguration, scopes, "mail");

                // Call the /me endpoint of MS Graph
                //await CallMSGraph(graphClient);

                //tokenToUse = msgraphToken;

                //MessageBox.Show("Before CallMSGraphSendMail", "IASharePoint Start - MainSendEmail", MessageBoxButtons.OK);


                await CallMSGraphSendMail(graphClient, emailSubject, emailBody, ToRecipientList,
                    CcRecipientList, BccRecipientList, FromEmail, attachments, EmailBodyHTML
                    , SaveToDraft, OpenEmailForm);

                //Console.ReadKey();


                //await SendMailAsync();

                //Environment.Exit(0);
                //System.Environment.Exit(-1);
            }

        }




        //// <GetUserSnippet>
        //public static Task<User> GetUserAsync()
        //{
        //    // Ensure client isn't null
        //    _ = Program.graphClient ??
        //        throw new System.NullReferenceException("Graph has not been initialized for user auth");

        //    return graphClient.Me
        //        .Request()
        //        .Select(u => new
        //        {
        //        // Only request specific properties
        //            u.DisplayName,
        //            u.Mail,
        //            u.UserPrincipalName
        //        })
        //        .GetAsync();
        //}
        //// </GetUserSnippet>

        //// <SendMailSnippet>
        //public static async Task SendMailAsync(string subject, string body, string recipient)
        //{
        //    // Ensure client isn't null
        //    _ = graphClient ??
        //        throw new System.NullReferenceException("Graph has not been initialized for user auth");

        //    // Create a new message
        //    var message = new Message
        //    {
        //        Subject = subject,
        //        Body = new ItemBody
        //        {
        //            Content = body,
        //            ContentType = BodyType.Text
        //        },
        //        ToRecipients = new Recipient[]
        //        {
        //        new Recipient
        //        {
        //            EmailAddress = new EmailAddress
        //            {
        //                Address = recipient
        //            }
        //        }
        //        }
        //    };

        //    // Send the message
        //    await graphClient.Me
        //        .SendMail(message)
        //        .Request()
        //        .PostAsync();
        //}
        //// </SendMailSnippet>

        //// <SendMailSnippet>
        //private async static Task SendMailAsync()
        //{
        //    try
        //    {
        //        // Send mail to the signed-in user
        //        // Get the user for their email address
        //        var user = await GetUserAsync();

        //        var userEmail = user?.Mail ?? user?.UserPrincipalName;

        //        if (string.IsNullOrEmpty(userEmail))
        //        {
        //            Console.WriteLine("Couldn't get your email address, canceling...");
        //            return;
        //        }

        //        await SendMailAsync("Testing Microsoft Graph",
        //            "Hello world!", userEmail);

        //        Console.WriteLine("Mail sent.");
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine($"Error sending mail: {ex.Message}");
        //    }
        //}
        // </SendMailSnippet>


        /// <summary>
        /// Sign in user using MSAL and obtain a token for MS Graph
        /// </summary>
        /// <returns></returns>
        private async static Task<GraphServiceClient> SignInAndInitializeGraphServiceClient(IPublicClientApplication application, PublicClientApplicationOptions configuration, string[] scopes, string connectionType)
        {

            //MessageBox.Show("Before Auth Token Cache", "IASharePoint Before - SignInAndInitializeGraphServiceClient", MessageBoxButtons.OK);


            if (msgraphToken == null)
            {
                //MessageBox.Show("Prior to SignInUserAndGetTokenUsingMSAL", "IASharePoint Message", MessageBoxButtons.OK);
                msgraphToken = await SignInUserAndGetTokenUsingMSAL(application, configuration, scopes, connectionType);
            }

            //if (msgraphToken != null)
            //{
            //    if (msgraphToken.Length == 0)
            //    {
            //        //Try again.
            //        msgraphToken = await SignInUserAndGetTokenUsingMSAL(application, configuration, scopes, connectionType);
            //    }
            //} else
            //{
            //    return null;
            //}

            //MessageBox.Show("Prior to GraphServiceClient", "IASharePoint Message", MessageBoxButtons.OK);

            try {
                GraphServiceClient graphClient = new(graphURL,
                            new DelegateAuthenticationProvider((requestMessage) => {
                    //requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", await SignInUserAndGetTokenUsingMSAL(application, configuration, scopes));
                                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", msgraphToken);
                                return Task.CompletedTask;
                            }));

                return await Task.FromResult(graphClient);
            }
            catch (Exception ex)
            {
                gUploadStatus = "IASharePoint Error Message - SignInAndInitializeGraphServiceClient";
                gStatusName = eBusyStatus.error;
                MessageBox.Show(ex.Message, "IASharePoint Error Message - SignInAndInitializeGraphServiceClient", MessageBoxButtons.OK);
                errorOccured = true;
                return null;
            }


        }

        public static void DeleteTokenCache()
        {
            TokenCacheHelper.DeleteTokenFile();
        }

        /// <summary>
        /// Signs in the user using the device code flow and obtains an Access token for MS Graph
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="scopes"></param>
        /// <returns></returns>
        private static async Task<string> SignInUserAndGetTokenUsingMSAL(IPublicClientApplication application, PublicClientApplicationOptions configuration, string[] scopes, string connectionType)
        {
            // build the AAd authority Url
            string authority = string.Concat(configuration.Instance, configuration.TenantId);
            //string authority = string.Concat(AzureCloudInstance.AzurePublic, configuration.TenantId);

            string ProgramName = "IASendMail";


            try {


                // Initialize the MSAL library by building a public client application
                //application = PublicClientApplicationBuilder.Create(configuration.ClientId)
                //                                   .WithAuthority(authority)
                //                                    .WithRedirectUri(CustomBrowserWebUi.FindFreeLocalhostRedirectUri()) // required for CustomBrowserWebUi
                //                                    .Build();

                // Initialize the MSAL library by building a public client application

                if (connectionType == "mail")
                {
                    ProgramName = "IASendEmail";
                    AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL", "Start", "Signing in to MSGraph", connectionType, "", "", "", "", false);
                    application = PublicClientApplicationBuilder.Create(configuration.ClientId)
                                                    .WithAuthority(authority)
                                                    //.WithBroker(true)
                                                    .WithRedirectUri(CustomBrowserWebUi.FindFreeLocalhostRedirectUri())
                                                    .Build();
                } else
                {
                    ProgramName = "IASharePoint";
                    AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL", "Start", "Signing in to MSGraph", connectionType, "", "", "", "", false);
                    application = PublicClientApplicationBuilder.Create(configuration.ClientId)
                                                    .WithAuthority(authority)
                                                    .WithBroker(true)
                                                    //.WithRedirectUri(CustomBrowserWebUi.FindFreeLocalhostRedirectUri())
                                                    .Build();
                }


                TokenCacheHelper.EnableSerialization(application.UserTokenCache);


                // Initialize the MSAL library by building a public client application
                //PublicClientApp = PublicClientApplicationBuilder.Create(ClientId)
                //    .WithAuthority(Authority)
                //    .WithBroker(true)
                //    //this is the currently recommended way to log MSAL message. For more info refer to https://github.com/AzureAD/microsoft-authentication-library-for-dotnet/wiki/logging
                //    .WithLogging(new IdentityLogger(EventLogLevel.Warning), enablePiiLogging: false) //set Identity Logging level to Warning which is a middle ground
                //    .Build();

                //_currentUserAccount = Task.Run(async () => await application.GetAccountsAsync()).Result.FirstOrDefault();


            }
            catch (Exception ex)
            {
                gUploadStatus = "Signing in to MSGraph - " + ex.Message;
                gStatusName = eBusyStatus.error;
                AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL_Exception", "Error", "Signing in to MSGraph - " + ex.Message, connectionType, "", "", "", "", false);

                MessageBox.Show(ex.Message, "IASharePoint Error Message - SignInUserAndGetTokenUsingMSAL", MessageBoxButtons.OK);
                errorOccured = true;
                return null;
            }

            AuthenticationResult result;

            //MessageBox.Show("Check Auth Token Cache", "IASharePoint Check Cache - SignInUserAndGetTokenUsingMSAL", MessageBoxButtons.OK);

            try
            {
                var accounts = await application.GetAccountsAsync();
                gUploadStatus = "Signing in to MSGraph with Existing Access Token";
                gStatusName = eBusyStatus.signIn;

                AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL_AcquireTokenSilent", "Start", "Signing in to MSGraph with Existing Access Token", connectionType, "", "", "", "", false);

                // Try to acquire an access token from the cache, if UI interaction is required, MsalUiRequiredException will be thrown.
                result = await application.AcquireTokenSilent(scopes, accounts.FirstOrDefault()).ExecuteAsync();

                AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL_AcquireTokenSilent", "Complete", "Completed Signing in to MSGraph with Existing Access Token", connectionType, "", "", "", "", false);

                return result.AccessToken;

            }
            catch (MsalUiRequiredException)
            {
                //MessageBox.Show("Prepare to Open Browser Scaopes " + scopes.ToString(), "IASharePoint Open Browser - SignInUserAndGetTokenUsingMSAL", MessageBoxButtons.OK);

                try
                {
                    gUploadStatus = "Signing in to MSGraph interactively using custom web UI";
                    gStatusName = eBusyStatus.signIn;
                    AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL_Interactively", "Start", "Signing in to MSGraph interactively using custom web UI", connectionType, "", "", "", "", false);
                    // Acquiring an access token interactively using custom web UI.
                    result = await application.AcquireTokenInteractive(scopes)
                            .WithCustomWebUi(new CustomBrowserWebUi()) //Using our custom web ui
                            .ExecuteAsync();
                    //MessageBox.Show("Open Browser Activated", "IASharePoint Open Browser Done - SignInUserAndGetTokenUsingMSAL", MessageBoxButtons.OK);


                    AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL_Interactively", "Complete", "Signing in to MSGraph interactively using custom web UI", connectionType, "", "", "", "", false);


                    return result.AccessToken;
                }
                catch (Exception ex)
                {
                    gUploadStatus = "Signing in to MSGraph - " + ex.Message;
                    gStatusName = eBusyStatus.error;
                    AddToLogFile(ProgramName, "SignInUserAndGetTokenUsingMSAL_Exception", "Error", "Signing in to MSGraph - " + ex.Message, connectionType, "", "", "", "", false);

                    MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")",
                        "IASharePoint Error Message - SignInUserAndGetTokenUsingMSAL", MessageBoxButtons.OK);
                    errorOccured = true;
                    return null;
                }
            }

        }




        public class MyConverter : JsonConverter
        {

            public override void WriteJson(JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
            {
                List<KeyValuePair<string, object>> list = value as List<KeyValuePair<string, object>>;
                writer.WriteStartArray();
                foreach (var item in list)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName(item.Key);
                    // Needed because of the dynamic object.
                    var jsonValue = JsonConvert.SerializeObject(item.Value);
                    writer.WriteValue(jsonValue);
                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(List<KeyValuePair<string, object>>);
            }
        }

        /// <summary>
        /// Call MS Graph and print results
        /// </summary>
        /// <param name="graphClient"></param>
        /// <returns></returns>
        private static async Task CallMSGraphMakeFolder(GraphServiceClient graphClient, string siteID, string selectedDriveID, string targetFolder
            )
        {

            gUploadStatus = "Making SharePoint Folder " + targetFolder;
            gStatusName = eBusyStatus.setupFolder;

            //var uploadedFile = _userClient.Sites["interacctsoftware.sharepoint.com:/sites/InterAcctUpdates"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"]
            try
            {

                string targetFolderTrimmed = Path.GetDirectoryName(targetFolder);

                string[] splitted = targetFolderTrimmed.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                var f = graphClient.Sites[siteID].Drive.Root;
                if (selectedDriveID.Length > 0)
                {
                    f = graphClient.Sites[siteID].Drives[selectedDriveID].Root;
                }

                string p = "";
                IDriveItemRequestBuilder b;
                DriveItem driveItem;
                foreach (string folder in splitted)
                {
                    p = string.Format("{0}{1}{2}", p, string.IsNullOrEmpty(p) ? "" : "/", folder);


                    if (selectedDriveID.Length > 0)
                    {
                        b = graphClient.Sites[siteID].Drives[selectedDriveID].Root.ItemWithPath(p);
                    } else
                    {
                        b = graphClient.Sites[siteID].Drive.Root.ItemWithPath(p);
                    }

                    try
                    {
                        driveItem = await b.Request().GetAsync();
                    }
                    catch
                    {
                        driveItem = null;
                    }
                    if (driveItem == null)
                    {
                        var f2 = await f.Children.Request().AddAsync(new DriveItem()
                        {
                            Name = folder,
                            Folder = new Folder
                            {

                            },
                            AdditionalData = new Dictionary<string, object>()
                                {
                                    {"@microsoft.graph.conflictBehavior", "rename"}
                                }
                        });

                        if (selectedDriveID.Length > 0)
                        {
                            b = graphClient.Sites[siteID].Drives[selectedDriveID].Root.ItemWithPath(p);
                        }
                        else
                        {
                            b = graphClient.Sites[siteID].Drive.Root.ItemWithPath(p);
                        }
                    }
                    f = b;
                }





                //var driveItem = new DriveItem
                //{
                //    Name = targetFolderTrimmed,
                //    Folder = new Folder
                //    {
                //    },
                //    AdditionalData = new Dictionary<string, object>()
                //    {
                //        {"@microsoft.graph.conflictBehavior", "rename"}
                //    }
                //};

                //if (selectedDriveID.Length > 0)
                //{
                //    await graphClient.Sites[siteID].Drives[selectedDriveID].Root.Children
                //    .Request()
                //    .AddAsync(driveItem);
                //} else
                //{
                //    await graphClient.Sites[siteID].Drive.Root.Children
                //    .Request()
                //    .AddAsync(driveItem);
                //}

            }
            catch (ServiceException serviceEx)
            {
                gUploadStatus = serviceEx.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(serviceEx.Message + " (" + serviceEx.StatusCode + ")",
                    "IASharePoint Error Message - CallMSGraphMakeFolder", MessageBoxButtons.OK);
                errorOccured = true;
            }
            catch (Exception ex)
            {
                gUploadStatus = ex.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")",
                    "IASharePoint Error Message - CallMSGraphMakeFolder", MessageBoxButtons.OK);
                errorOccured = true;
            }

            return;

        }



        /// <summary>
        /// Call MS Graph and print results
        /// </summary>
        /// <param name="graphClient"></param>
        /// <returns></returns>
        private static async Task CallMSGraphMakeDrive(GraphServiceClient graphClient, string siteID, string driveName
            )
        {

            gUploadStatus = "Making SharePoint Drive " + driveName;
            gStatusName = eBusyStatus.setupFolder;

            //var uploadedFile = _userClient.Sites["interacctsoftware.sharepoint.com:/sites/InterAcctUpdates"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"]
            try
            {
                var driveItem = new List
                {
                    DisplayName = driveName,
                    ListInfo = new ListInfo
                    {
                        Template = "documentLibrary", Hidden = false, ContentTypesEnabled = true
                    },
                    //AdditionalData = new Dictionary<string, object>()
                    //    {
                    //        {"@microsoft.graph.conflictBehavior","rename"}
                    //    }
                };

                await graphClient.Sites[siteID].Lists
    .Request()
    .AddAsync(driveItem);


                //var driveItem = new Drive
                //{
                //    Name = driveName,
                //    Description = driveName,
                //    AdditionalData = new Dictionary<string, object>()
                //        {
                //            {"@microsoft.graph.conflictBehavior","rename"}
                //        }
                //};

                //await graphClient.Sites[siteID].Drives
                //.Request()
                //.AddAsync(driveItem).ConfigureAwait(false);

                // await graphClient.Sites[siteID].Drives
                //    .Request()
                //      .AddAsync(driveItem);


            }
            catch (ServiceException serviceEx)
            {
                gUploadStatus = serviceEx.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(serviceEx.Message + " (" + serviceEx.StatusCode + ")",
                    "IASharePoint Error Message - CallMSGraphMakeDrive", MessageBoxButtons.OK);
                errorOccured = true;
            }
            catch (Exception ex)
            {
                gUploadStatus = ex.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")",
                    "IASharePoint Error Message - CallMSGraphMakeDrive", MessageBoxButtons.OK);
                errorOccured = true;
            }

            return;

        }


        /// <summary>
        /// Call MS Graph and print results
        /// </summary>
        /// <param name="graphClient"></param>
        /// <returns></returns>
        private static async Task<string> CallMSGraphGetDrives(GraphServiceClient graphClient, string siteID
            )
        {

            gUploadStatus = "Getting Drives on SharePoint. " + siteID;
            gStatusName = eBusyStatus.setupFolder;


            //var uploadedFile = _userClient.Sites["interacctsoftware.sharepoint.com:/sites/InterAcctUpdates"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"]
            try
            {
                var result = await graphClient.Sites[siteID].Drives.Request().GetAsync();

                JsonSerializerSettings settings = new() { Converters = new[] { new MyConverter() } };
                string json = JsonConvert.SerializeObject(result.CurrentPage, settings);

                return json;


            }
            catch (ServiceException serviceEx)
            {
                gUploadStatus = serviceEx.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(serviceEx.Message + " (" + serviceEx.StatusCode + ")",
                    "IASharePoint Error Message - CallMSGraphGetDrives", MessageBoxButtons.OK);
                errorOccured = true;
            }
            catch (Exception ex)
            {
                gUploadStatus = ex.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")",
                    "IASharePoint Error Message - CallMSGraphGetDrives", MessageBoxButtons.OK);
                errorOccured = true;
            }

            return null; ;

        }


        public static async Task<DriveItem> UploadToFolder(
            GraphServiceClient graphClient,
            string siteID,
       string selectedDriveID,
       //string folderId,
       string filePath,
       string TargetFolder)
        {
            DriveItem resultDriveItem = null;

            gUploadStatus = "Uploading File to Folder " + filePath + " to "+ selectedDriveID;
            gStatusName = eBusyStatus.sendingFile;

            AddToLogFile("IASharePoint", "UploadToFolder", "Start", "Upload Large File to SharePoint", filePath, "", siteID, "", selectedDriveID, false);

            try
            {
                using (Stream fileStream = new FileStream(
                        filePath,
                        FileMode.Open,
                        FileAccess.Read, FileShare.Read))
                {


                    //var startPart = graphClient.Sites[siteID].Drive.Items[folderId].Root;
                    var startPart = graphClient.Sites[siteID].Drive.Root;
                    if (selectedDriveID.Length > 0)
                    {
                        //startPart = graphClient.Sites[siteID].Drives[selectedDriveID].Items[folderId];
                        startPart = graphClient.Sites[siteID].Drives[selectedDriveID].Root;
                    }


                    var uploadSession = await startPart.ItemWithPath(TargetFolder).CreateUploadSession().Request().PostAsync();

                    int maxSlice = 320 * 1024;

                    var largeFileUpload = new LargeFileUploadTask<DriveItem>(uploadSession, fileStream, maxSlice);

                    IProgress<long> progress = new Progress<long>(x =>
                    {
                        //_logger.LogDebug($"Uploading large file: {x} bytes of {fileStream.Length} bytes already uploaded.");
                    });

                    UploadResult<DriveItem> uploadResult = await largeFileUpload.UploadAsync(progress);

                    resultDriveItem = uploadResult.ItemResponse;
                }

                AddToLogFile("IASharePoint", "UploadToFolder_Completed", "Complete", "Completed Upload Large File to SharePoint", filePath, "", siteID, "", selectedDriveID, false);


                return resultDriveItem;

            }
            catch (Exception ex)
            {
                gUploadStatus = "Failed to Upload Large File to SharePoint - " + ex.Message;
                gStatusName = eBusyStatus.error;
                AddToLogFile("IASharePoint", "UploadToFolder_Failed", "Error", "Failed to Upload Large File to SharePoint - " + ex.Message, filePath, "", siteID, "", selectedDriveID, false);

                MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")" + Environment.NewLine + Environment.NewLine +
                    "Source File Path : " + filePath + Environment.NewLine +
                    "SharePoint Path: " + TargetFolder + Environment.NewLine +
                    "Site ID: " + siteID,
                    "IASharePoint Error Message - UploadToFolder", MessageBoxButtons.OK);
                errorOccured = true;
            }

            return null;

        }


        /// <summary>
        /// Call MS Graph and print results
        /// </summary>
        /// <param name="graphClient"></param>
        /// <returns></returns>
        private async static void CallMSGraphSendSharePoint(GraphServiceClient graphClient, string filePath, string fileName, string siteID, string TargetFolder, string selectedDriveID, bool deleteAfterSaving
            )
        {
            //var me = await graphClient.Me.Request().GetAsync();


            //MessageBox.Show("Prior to Path Combine", "IASharePoint Message",  MessageBoxButtons.OK);




            //MessageBox.Show("Preparing to send " + Environment.NewLine + "filename :" + fileName + Environment.NewLine + "from Path " + filePath + Environment.NewLine + "to  " + TargetFolder + Environment.NewLine + "on Server " + siteID, "IASharePoint Message", MessageBoxButtons.OK);


            
            //Move to Sub Folder
            string fileNewPath = TargetFolder + fileName;

            // get a stream of the local file
            FileStream fileStream = new(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            gFileUploaded = false;

            long fileLength = 0;
            try
            {

                
                bool fileStreamIsNotNull = (fileStream.Length > 0);

                FileInfo fi = new(filePath);
                fileLength = fi.Length;

            }
            catch (Exception ex)
            {
                gUploadStatus = "Error - " + ex.Message;
                gStatusName = eBusyStatus.error;
                MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")" + Environment.NewLine +
                    "Source File Name : " + fileName + Environment.NewLine +
                    "Source File Path : " + filePath + Environment.NewLine +
                    "SharePoint Path: " + fileNewPath + Environment.NewLine +                    
                    "Site ID: " + siteID,
                    "IASharePoint Error Message - Get File Size", MessageBoxButtons.OK);
                errorOccured = true;
            }





            //Check if the file is over 4MB, if so, run this function instead.
            if (fileLength > 4194303)
            {
                gUploadStatus = "Uploading File over 4MB to Folder " + filePath + " to " + selectedDriveID;
                gStatusName = eBusyStatus.sendingFile;

                var uploadedFile = await UploadToFolder(graphClient, siteID, selectedDriveID, filePath, fileNewPath);


                if (uploadedFile != null)
                {
                    gFileUploaded = true;
                    AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint_Completed", "Complete", "Upload File over 4MB With Drive ID", filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);

                    if (deleteAfterSaving)
                    {
                        if (System.IO.File.Exists(fileNewPath))
                        {
                            // Delete the file
                            System.IO.File.Delete(fileNewPath);
                        }
                    }

                    //MessageBox.Show(uploadedFile.ToString() + Environment.NewLine + "Source File Name : " + fileName + Environment.NewLine + "Source File Path : " + filePath + Environment.NewLine + "SharePoint Path: " + fileNewPath + Environment.NewLine + "Site ID: " + siteID, "IASharePoint - CallMSGraphSendSharePoint - File Uploaded", MessageBoxButtons.OK);
                    //Console.WriteLine($"Upload File Result: {uploadedFile}");
                }
                return;
            }






            //var uploadedFile = _userClient.Sites["interacctsoftware.sharepoint.com:/sites/InterAcctUpdates"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"].Drives["b!9vreBEDFA02AFsyH9hm2d-R1SR9yyeBDlaH0XHhoP2D1wxi3Gm59SJsC7xwyn_m-"]
            try {


                if (selectedDriveID.Length > 0)
                {

                    gUploadStatus = "Uploading File Folder " + filePath + " to " + selectedDriveID;
                    gStatusName = eBusyStatus.sendingFile;

                    AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint", "Start","Upload File under 4MB With Drive ID", filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);

                    var uploadedFile = graphClient.Sites[siteID].Drives[selectedDriveID].Root
                                          .ItemWithPath(fileNewPath)
                                          .Content
                                          .Request()
                                          .PutAsync<DriveItem>(fileStream)
                                          .Result;

                    if (uploadedFile != null)
                    {
                        gFileUploaded = true;
                        AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint_Completed", "Complete", "Upload File under 4MB With Drive ID", filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);

                        if (deleteAfterSaving)
                        {
                            if (System.IO.File.Exists(fileNewPath))
                            {
                                // Delete the file
                                System.IO.File.Delete(fileNewPath);
                            }
                        }

                        //MessageBox.Show(uploadedFile.ToString() + Environment.NewLine + "Source File Name : " + fileName + Environment.NewLine + "Source File Path : " + filePath + Environment.NewLine + "SharePoint Path: " + fileNewPath + Environment.NewLine + "Site ID: " + siteID, "IASharePoint - CallMSGraphSendSharePoint - File Uploaded", MessageBoxButtons.OK);
                        //Console.WriteLine($"Upload File Result: {uploadedFile}");
                    }
                } else
                {

                    AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint", "Start", "Upload File under 4MB With No Drive ID", filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);

                    var uploadedFile = graphClient.Sites[siteID].Drive.Root
                                          .ItemWithPath(fileNewPath)
                                          .Content
                                          .Request()
                                          .PutAsync<DriveItem>(fileStream)
                                          .Result;

                    if (uploadedFile != null)
                    {
                        gFileUploaded = true;
                        AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint_Completed", "Complete", "Upload File under 4MB With No Drive ID", filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);

                        if (deleteAfterSaving)
                        {
                            if (System.IO.File.Exists(fileNewPath))
                            {
                                // Delete the file
                                System.IO.File.Delete(fileNewPath);
                            }
                        }

                        //MessageBox.Show(uploadedFile.ToString() + Environment.NewLine + "Source File Name : " + fileName + Environment.NewLine + "Source File Path : " + filePath + Environment.NewLine + "SharePoint Path: " + fileNewPath + Environment.NewLine + "Site ID: " + siteID, "IASharePoint - CallMSGraphSendSharePoint - File Uploaded", MessageBoxButtons.OK);
                        //Console.WriteLine($"Upload File Result: {uploadedFile}");
                    }
                }








            }
            catch (ServiceException serviceEx)
            {
                gUploadStatus = "Failed to upload File under 4MB - " + serviceEx.Message;
                gStatusName = eBusyStatus.error;
                AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint_ServiceException", "Error", "Failed to upload File under 4MB - " + serviceEx.Message, filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);



                MessageBox.Show(serviceEx.Message + " (" + serviceEx.StatusCode + ")" + Environment.NewLine +
                    "Source File Name : " + fileName + Environment.NewLine +
                    "Source File Path : " + filePath + Environment.NewLine +
                    "SharePoint Path: " + fileNewPath + Environment.NewLine +
                    "Site ID: " + siteID
                    , "IASharePoint Error Message - CallMSGraphSendSharePoint", MessageBoxButtons.OK);
                errorOccured = true;
            }            
            catch (Exception ex)
            {
                gUploadStatus = "Failed to upload File under 4MB - " + ex.Message;
                gStatusName = eBusyStatus.error;
                AddToLogFile("IASharePoint", "CallMSGraphSendSharePoint_Exception", "Error", "Failed to upload File under 4MB - " + ex.Message, filePath, fileName, siteID, TargetFolder, selectedDriveID, deleteAfterSaving);

                MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")" + Environment.NewLine + 
                    "Source File Name : " + fileName + Environment.NewLine + 
                    "Source File Path : " + filePath + Environment.NewLine + 
                    "SharePoint Path: " + fileNewPath + Environment.NewLine +                     
                    "Site ID: " + siteID, 
                    "IASharePoint Error Message - CallMSGraphSendSharePoint", MessageBoxButtons.OK);
                errorOccured = true;
            }

}





        /// <summary>
        /// Call MS Graph Send Email
        /// </summary>
        /// <param name="graphClient"></param>
        /// <returns></returns>
        private static async Task CallMSGraphSendMail(GraphServiceClient graphClient, string emailSubject, string emailBody, List<Microsoft.Graph.Recipient> ToRecipientList
            , List<Microsoft.Graph.Recipient> CcRecipientList
            , List<Microsoft.Graph.Recipient> BccRecipientList
            , Microsoft.Graph.Recipient FromEmail
            , Microsoft.Graph.MessageAttachmentsCollectionPage attachments
            , string EmailBodyHTML
            , bool SaveToDraft
            , bool OpenEmailForm
            )
        {
            //var me = await graphClient.Me.Request().GetAsync();

            BodyType contentType = BodyType.Text;


            AddToMailLogFile("IASendEmail", "CallMSGraphSendMail", "Start", "Prepare to Send Email", emailSubject, ToRecipientList, CcRecipientList, FromEmail, attachments, SaveToDraft);

            if (EmailBodyHTML != null)
            {
                emailBody = EmailBodyHTML;
                contentType = BodyType.Html;
            }

            var importance = Importance.Normal;


            Microsoft.Graph.Message thisMessage = new()
            {
                Subject = emailSubject,
                Importance = importance,
                Body = new ItemBody
                {
                    ContentType = contentType,
                    Content = emailBody
                },
                ToRecipients = ToRecipientList,
                CcRecipients = CcRecipientList,
                BccRecipients = BccRecipientList,
                From = FromEmail,
                Attachments = attachments
            };




            if (SaveToDraft)
            {

                try
                {


                    await graphClient.Me.Messages
        .Request()
        .AddAsync(thisMessage);

                    AddToMailLogFile("IASendEmail", "CallMSGraphSendMail", "Complete", "Email to saved to Draft", emailSubject, ToRecipientList, CcRecipientList, FromEmail, attachments, SaveToDraft);


                    if (OpenEmailForm)
                    {
                        //TO-DO: This needs to create a Outlook like form, and then Open it up to allow to send.

                        //                var openMessage = await graphClient.Me.Messages[draftMessage.Id]
                        //.Request()
                        //.GetAsync();

                    }
                }
                catch (Exception ex)
                {
                    gUploadStatus = "Failed to Save Email to Draft - " + ex.Message;
                    gStatusName = eBusyStatus.error;
                    AddToMailLogFile("IASendEmail", "CallMSGraphSendMail", "Error", "Failed to Save Email to Draft", emailSubject, ToRecipientList, CcRecipientList, FromEmail, attachments, SaveToDraft);

                    MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")" + Environment.NewLine +
                    "Email Subject : " + emailSubject + Environment.NewLine,
                    "IASendEmail Error Message - CallMSGraphSendMail - Save to Draft", MessageBoxButtons.OK);
                    errorOccured = true;
                    //"From Email : " + FromEmail.ToString() + Environment.NewLine +
                    //"To Recipient: " + ToRecipientList.ToString() + Environment.NewLine +
                    //"Attachments: " + attachments.ToString(),

                }




            }
            else
            {

                try
                {
                    var saveToSentItems = true;

                    await graphClient.Me
        .SendMail(thisMessage, saveToSentItems)
        .Request()
        .PostResponseAsync();


                    AddToMailLogFile("IASendEmail", "CallMSGraphSendMail", "Complete", "Email Sent", emailSubject, ToRecipientList, CcRecipientList, FromEmail, attachments, SaveToDraft);

                }

                catch (Exception ex)
                {
                    gUploadStatus = "Failed to Send Email - " + ex.Message;
                    gStatusName = eBusyStatus.error;
                    AddToMailLogFile("IASendEmail", "CallMSGraphSendMail", "Error", "Failed to Send Email", emailSubject, ToRecipientList, CcRecipientList, FromEmail, attachments, SaveToDraft);

                    MessageBox.Show(ex.Message + Environment.NewLine + " (" + ex.InnerException + ")" + Environment.NewLine +
                    "Email Subject : " + emailSubject + Environment.NewLine,
                    "IASendEmail Error Message - CallMSGraphSendMail - Send Email", MessageBoxButtons.OK);
                    errorOccured = true;

                    //"From Email : " + FromEmail.ToString() + Environment.NewLine +
                    //"To Recipient: " + ToRecipientList.ToString() + Environment.NewLine +
                    //"Attachments: " + attachments.ToString(),

                }





                //.PostAsync();


                // Printing the results
                Console.Write(Environment.NewLine);
                Console.WriteLine("-------- Message Sent --------");


                //Console.Write(Environment.NewLine);
                //Console.WriteLine($"Id: {me.Id}");
                //Console.WriteLine($"Display Name: {me.DisplayName}");
                //Console.WriteLine($"Email: {me.Mail}");
            }
        }




        /// <summary>
        /// Call MS Graph and print results
        /// </summary>
        /// <param name="graphClient"></param>
        /// <returns></returns>
        //private static async Task CallMSGraph(GraphServiceClient graphClient)
        //{
        //    var me = await graphClient.Me.Request().GetAsync();

        //    // Printing the results
        //    Console.Write(Environment.NewLine);
        //    Console.WriteLine("-------- Data from call to MS Graph --------");
        //    Console.Write(Environment.NewLine);
        //    Console.WriteLine($"Id: {me.Id}");
        //    Console.WriteLine($"Display Name: {me.DisplayName}");
        //    Console.WriteLine($"Email: {me.Mail}");
        //}



        public static async Task Logout(IPublicClientApplication application)
        {

            
            var accounts = await application.GetAccountsAsync();
            if (accounts.Any())
            {
                try
                {
                    await application.RemoveAsync(accounts.FirstOrDefault());                    
                }
                catch (MsalException ex)
                {
                    gUploadStatus = "Failed to LogOut- " + ex.Message;
                    gStatusName = eBusyStatus.error;
                    throw new Exception($"Error signing-out user: {ex.Message}");
                }
            }
        }

    }


}
