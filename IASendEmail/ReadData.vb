﻿Imports System.IO
Imports System.IO.FileStream
Imports System.Text
Imports System.Security.Cryptography

Module ReadData

    Dim CurrentReportRef As String = ""
    Dim ReportIndex As Integer = -1
    Dim EmailIndex As Integer = -1
    Dim AttachIndex As Integer = -1
    Dim CreatePDFIndex As Integer = -1
    Public ReportFileName As String = ""

    Dim ShowAttachFileErrors As Boolean = True


    Dim RptFilePath As String = Trim(System.Windows.Forms.Application.StartupPath)
    Dim AttachPath As String = RptFilePath
    Public SelectedReportFolder As String = ""

    ReadOnly SystemTitle As String = "InterAcct Software"
    ReadOnly gVersionNumber As String = Application.ProductVersion
    Public ApplicationTitle As String = String.Format("{0} - {1} - Version : {2} ", SystemTitle, My.Application.Info.ProductName, gVersionNumber)

    Public Const MaxEmailAddresses = 50   'PWR 18.11.2008 Was 1,000 but it seems a bit of an over kill
    Public Const MaxAttachFiles = 50   'PWR 18.11.2008 Was 1,000 but it seems a bit of an over kill
    Public Const MaxPages = 2500        'PWR 27.01.2009 Increased the Maximium Pages to 2500 Pages from 1000 Pages

    Public SaveToDraft As Boolean = False
    Public OpenEmailForm As Boolean = False
    Public OpenEmailFormCommand As Boolean = False


    Const GlobalKey As String = "gXL6tkTjyy#BhcYgg?ncP?6Pgx@i@&4qPe7tJN"

    Private DES As New TripleDESCryptoServiceProvider
    Private MD5 As New MD5CryptoServiceProvider

    Public Function MD5Hash(ByVal value As String) As Byte()
        Return MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(value))
    End Function

    Public Function Encrypt(ByVal stringToEncrypt As String) As String
        Try
            DES.Key = MD5Hash(GlobalKey)
            DES.Mode = CipherMode.ECB
            Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(stringToEncrypt)
            Return Convert.ToBase64String(DES.CreateEncryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))
        Catch ex As Exception
            MessageBox.Show("Invalid Key", "Encryption Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return ""
        End Try
    End Function

    Public Function Decrypt(ByVal encryptedString As String) As String
        Try
            DES.Key = MD5Hash(GlobalKey)
            DES.Mode = CipherMode.ECB
            Dim Buffer As Byte() = Convert.FromBase64String(encryptedString)
            Return ASCIIEncoding.ASCII.GetString(DES.CreateDecryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))
        Catch ex As Exception
            MessageBox.Show("Invalid Key", "Decryption Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return ""
        End Try
    End Function

    'Public Function base64Encode(sData As String) As String
    '    Try
    '        Dim encData_byte As Byte() = New Byte(sData.Length - 1) {}

    '        encData_byte = System.Text.Encoding.UTF8.GetBytes(sData)

    '        Dim encodedData As String = Convert.ToBase64String(encData_byte)


    '        Return encodedData
    '    Catch ex As Exception
    '        Throw New Exception("Error in base64Encode" + ex.Message)
    '    End Try
    'End Function
    ''Method To Decode Password

    'Public Function base64Decode(sData As String) As String

    '    Dim encoder As New System.Text.UTF8Encoding()

    '    Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()

    '    Dim todecode_byte As Byte() = Convert.FromBase64String(sData)

    '    Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)

    '    Dim decoded_char As Char() = New Char(charCount - 1) {}

    '    utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)

    '    Dim result As String = New [String](decoded_char)

    '    Return result

    'End Function


    Public Structure AttachType
        Dim Name As String              'This is the name on the file when the file is attached to the email (Eg:"Invoice")
        Dim File As String              'This is the Actual File Name (Eg:Invoice.doc)
        Dim Directory As String         'This is the Directory Location where the Attachment can be found.
        Dim SecureReport As Boolean    'This Indicates if we are going to delete the file after it has been sent.
    End Structure

    Structure PageType
        '! Dead type field: PageNo
        Dim PageNo As Integer
        Dim LineNo As Long           'This is the Number of Lines on this Page.
        Dim StartLine As Long        'This is the First Line Number of this Page from the Total Document
    End Structure

    Public Structure EmailType
        Dim EmailReceipt As String      'This is the Email Address
        Dim EmailFullName As String     'This is the Full Name that you want to send to.
        Dim EmailSend As Integer        'To Send as 1.TO or 2.CC or 3.BCC
    End Structure

    Public Structure ReportType
        Dim FileName As String          'This is the Filename of the Report No Extention and No Path.
        Dim FileReference As String    'This is the Reference of this Individual Report.
        '! Type field written, not read: FileOwners - 0 reads, 1 write
        Dim FileOwners As String        'This is the Owners Details of the Report... Who is Printing it.

        Dim EmailIndexCount As Integer              'This is a count of the Email Addresses in this Type
        Dim Email() As EmailType   'These are the details of each email Address
        Dim MainEmailAddress As String              'This is the First Email Address in the list
        Dim EmailSubject As String                  'This is the Subject of the Email
        Dim EmailBody As String                     'This is the Body of the Email

        Dim EmailFrom As String                     'PWR 27.01.2017 Email From
        Dim EmailReplyTo As String                  'PWR 31.10.2017 Email Reply To

        Dim AttachIndexCount As Integer             'This is the count of the extra Attachments
        Dim Attach() As AttachType    'These are the Attached Files added to the email.

        Dim CreatePDFCount As Integer
        Dim PDFFiles() As AttachType  'PWR 16.04.2012 This allows to create multiple PDF Document File names in multiple locations for a single report.

        Dim Printer As String                       'PWR 29.05.2015 Allow for multiple Printers and Trays in a Single Report
        Dim Tray As String                          'PWR 29.05.2015 Allow for multiple Printers and Trays in a Single Report

        Dim FaxNumber As String                     'This is the Fax Number of the Addressee of the Report.

        Dim PageStart As Integer                    'This is the Start Page for the Full Run of Reports in the File.
        Dim PageEnd As Integer                      'This is the end Page for the Full Run of Reports in the File
        Dim PageCount As Integer                    'This is the Total Number of Pages for this Report.

        Dim PageLines() As PageType
    End Structure

    Public Report() As ReportType

    Public Sub Report_ReDim_Structure()
        ReDim Report(ReportIndex).PageLines(MaxPages)
        ReDim Report(ReportIndex).PDFFiles(MaxAttachFiles)
        ReDim Report(ReportIndex).Attach(MaxAttachFiles)
        ReDim Report(ReportIndex).Email(MaxEmailAddresses)
        Report(ReportIndex).MainEmailAddress = ""
        Report(ReportIndex).FaxNumber = ""
        Report(ReportIndex).EmailBody = ""
        Report(ReportIndex).EmailFrom = ""
        Report(ReportIndex).EmailReplyTo = ""
        Report(ReportIndex).EmailSubject = ""
        Report(ReportIndex).Printer = ""
        Report(ReportIndex).Tray = ""
        Report(ReportIndex).EmailIndexCount = -1
    End Sub


    Public Function CheckDocumentExists(ByRef DocFile As String) As Boolean
        If System.IO.File.Exists(DocFile) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function SetFilePath(ByRef DirName As String, ByRef FileName As String) As String
        'PWR 08.06.2011 Simpler Rotuien to set the FIle Path/Routine

        If DirName = vbNullString Then
            If SelectedReportFolder = "" Then
                DirName = RptFilePath
            Else
                DirName = SelectedReportFolder
            End If
        End If

            If Mid$(FileName, 2, 1) = ":" Then        'Make it so you can just have a Filename
                SetFilePath = FileName
            ElseIf Mid$(FileName, 1, 2) = "\\" Then        'Make it so you can just have a Filename 'PWR 04.02.2008 Fix for \\ File Names
                SetFilePath = FileName
            Else
                If Right$(DirName, 1) = "\" And Left$(FileName, 1) = "\" Then     'PWR 14.09.2012 Set the Folder without the Double Slashes
                    SetFilePath = DirName & Mid$(FileName, 2)
                Else
                    SetFilePath = DirName & FileName
                End If
            End If

    End Function


    Public Sub NewReport()
        If (ReportIndex = -1) Or (Report(ReportIndex).FileReference <> CurrentReportRef And CurrentReportRef <> "" And Report(ReportIndex).FileReference.Length <> 0) Then        'PWR 17.10.2004 CHanged from -1
            ReportIndex = ReportIndex + 1               'PWR 17.10.2004 Adding here rather than CreateNow

            If UBound(Report) < ReportIndex Then
                ReDim Preserve Report(ReportIndex + 10)
            End If

            EmailIndex = -1
            AttachIndex = -1
            CreatePDFIndex = -1
            CleanReportRef(CurrentReportRef)
            Report_ReDim_Structure()
            Report(ReportIndex).FileReference = CurrentReportRef
        End If

    End Sub

    Public Function WarningMessage_Ask(ByVal ErrDetails As String, ByVal ErrHeading As String) As Integer
        WarningMessage_Ask = MsgBox(ErrDetails, vbYesNo, ApplicationTitle & ErrHeading)
    End Function

    Public Sub CleanReportRef(ByVal xString As String)

        Dim x As Integer

        For x = 1 To 47
            If InStr(1, xString, ChrW(x)) <> 0 Then
                xString = Replace(xString, ChrW(x), "_")
            End If
        Next

        For x = 58 To 64
            If InStr(1, xString, ChrW(x)) <> 0 Then
                xString = Replace(xString, ChrW(x), "")
            End If
        Next

        For x = 123 To 255

            If InStr(1, xString, ChrW(x)) <> 0 Then
                xString = Replace(xString, ChrW(x), "")
            End If
        Next

    End Sub

    Public Function GetQuote(ByVal Tag As String, ByVal Src As String) As String
        Dim n As Integer
        Dim txt As String
        'Src = LCase$(Src)
        'Tag = LCase$(Tag)
        txt = vbNullString
        n = InStr(LCase$(Src), LCase$(Tag))
        If n > 0 Then
            '! Unicode function is faster: ChrW$
            n = InStr(n, LCase$(Src), Chr(34))
            If n > 0 Then
                txt = Right$(Src, Src.Length - n)
                '! Unicode function is faster: ChrW$
                n = InStr(txt, Chr(34))
                If n > 0 Then
                    txt = Left$(txt, n - 1)
                Else
                    txt = vbNullString
                End If
            End If
        End If
        GetQuote = txt
    End Function

    Function Find_Folder(ByVal TestFileName As String) As String

        Dim BaseFileName As String = TestFileName

        'Find the Base Filename just incase the File doesn't exist in teh current location.
        Dim Ptr As Integer = -1
        Ptr = InStrRev(TestFileName, "\")
        If Ptr > 0 Then
            BaseFileName = Mid(TestFileName, (Ptr + 1))
        End If

        If (Mid$(TestFileName, 2, 1) = ":") Or (Mid$(TestFileName, 1, 2) = "\\") Then        'Make it so you can just have a Filename
            Find_Folder = TestFileName
        Else
            If SelectedReportFolder = "" Then
                Find_Folder = (RptFilePath & TestFileName)
            Else
                Find_Folder = (SelectedReportFolder & TestFileName)
            End If
        End If

        If Not CheckDocumentExists(Find_Folder) Then
            Find_Folder = (RptFilePath & BaseFileName)
            If Not CheckDocumentExists(Find_Folder) Then
                Find_Folder = (RptFilePath & "Reports\" & BaseFileName)
                If Not CheckDocumentExists(Find_Folder) Then
                    Find_Folder = (SelectedReportFolder & BaseFileName)
                    If Not CheckDocumentExists(Find_Folder) Then
                        Find_Folder = (SelectedReportFolder & "Reports\" & BaseFileName)
                        If Not CheckDocumentExists(Find_Folder) Then
                            Find_Folder = ""
                        End If
                    End If
                End If
            End If
        End If

        'MsgBox("Found Report " & Find_Folder, MessageBoxButtons.OK, "IASendEmail - Found Report")

        If (Find_Folder.Length > 0) Then        'PWR 08.12.2022 Change the Default Folder Name
            Ptr = InStrRev(Find_Folder, "\")
            If Ptr > 0 Then
                RptFilePath = Mid(Find_Folder, 1, (Ptr - 1))
            End If
        End If


    End Function


    Public Sub Open_Report_File()
        SaveToDraft = False         'Reset back to default.
        OpenEmailForm = False       'PWR 19.02.2016
        Form1.OpenFileDialog1.InitialDirectory = RptFilePath
        Form1.OpenFileDialog1.Filter = "report files (*.rpt)|*.rpt|txt files (*.txt)|*.txt|All files (*.*)|*.*"
        Form1.OpenFileDialog1.FilterIndex = 1
        Form1.OpenFileDialog1.RestoreDirectory = True
        Form1.OpenFileDialog1.FileName = ""

        If Form1.OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try

            Catch Ex As Exception
                'MessageBox.Show("Cannot read file from disk. Original error: " & Ex.Message)
                AddtoErrorLog("ReadData", "Open_Report_File", Ex.Message, "Cannot read file from disk. Original error")
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.                     
            End Try
        End If
    End Sub

    Public Sub Load_Program()

        If Report Is Nothing Then    'Only need to load once, or if the data has been removed.

            If Mid(RptFilePath, RptFilePath.Length, 1) <> "\" Then
                RptFilePath = RptFilePath & "\"
            End If

            Dim Commands() As String = Environment.GetCommandLineArgs()
            If ReportFileName.Length = 0 Then
                If Commands.Length > 1 Then
                    'This is the Filename with all the instructions for the report.
                    ReportFileName = Trim(Commands(1))
                    ReportFileName = Replace(ReportFileName, Chr(34), "")           'PWR 06.02.2017 Remove the Quotes around the directory name.

                    If Commands.Length >= 3 Then        'PWR 17.12.2018 Needs to have 3 elements if there is a Condition.
                        If Commands(2).ToString.Length > 1 Then     'PWR 17.12.2018

                            If UCase$(Mid(Commands(2).ToString, 1, 3)) = "\DO" Or
                                UCase$(Mid(Commands(2).ToString, 1, 3)) = "/DO" Or
                                UCase$(Mid(Commands(2).ToString, 1, 3)) = "\OD" Or
                                UCase$(Mid(Commands(2).ToString, 1, 3)) = "/OD" Then      'PWR 19.02.2016
                                SaveToDraft = True
                                'MsgBox("/OD COmmand to Save to Draft")
                                OpenEmailForm = True
                                OpenEmailFormCommand = True
                            Else
                                If UCase$(Mid(Commands(2).ToString, 1, 2)) = "\D" Or UCase$(Mid(Commands(2).ToString, 1, 2)) = "/D" Then      'PWR 31.08.2015
                                    OpenEmailForm = False
                                    'MsgBox("/D COmmand to Save to Draft" + Commands(2).ToString)
                                    SaveToDraft = True
                                End If
                                If UCase$(Mid(Commands(2).ToString, 1, 2)) = "\O" Or UCase$(Mid(Commands(2).ToString, 1, 2)) = "/O" Then      'PWR 19.02.2016
                                    OpenEmailForm = True
                                    OpenEmailFormCommand = True
                                End If
                            End If
                        End If

                    End If

                Else
                    Open_Report_File()
                End If
            End If

            If ReportFileName.Length <> 0 Then
                ReportFileName = Find_Folder(ReportFileName)
                If ReportFileName.Length = 0 Then
                    MsgBox("File does not exist : " & Trim(Commands(1)), vbOKOnly, "Report File doesn't Exist")
                    Exit Sub
                Else
                    ReadData_RPT()
                End If
            End If

        End If

        'MsgBox("Load_Program Finished", MessageBoxButtons.OK, "IASendEmail - Load_Program")

    End Sub


    Sub ReadData_RPT()

        If ReportFileName.Length = 0 Then
            Exit Sub
        End If

        If ReportIndex = -1 Then        'PWR 10.08.2005 Moved Above Prior Routine
            ReDim Report(10)
            ReportIndex = 0
            Report(0).PageStart = 1
            Report(0).FileReference = ""
            Report(0).EmailIndexCount = -1
            Report_ReDim_Structure()
            'Report(0).PageCount = CPage
        End If

        Dim LineText As String = ""

        Dim filePath As String = System.IO.Path.GetFullPath(ReportFileName)

        'MsgBox("Read Report File Name :" + ReportFileName + Environment.NewLine +
        '      "Path :" + filePath + Environment.NewLine, MessageBoxButtons.OK, "IASendEmail - Checking Report Data")

        If (filePath.Length > 0) Then
            Dim Ptr As Integer = InStrRev(filePath, "\")
            If Ptr > 0 Then
                RptFilePath = Mid(filePath, 1, (Ptr - 1))
            End If
        End If

        Directory.SetCurrentDirectory(Application.StartupPath)

        Dim CurDir As String = Directory.GetCurrentDirectory()

        'MsgBox("CurDir :" + CurDir, MessageBoxButtons.OK, "IASendEmail - Currrent Directory")

        'Using reader As StreamReader = New StreamReader(ReportFileName)
        Using reader As StreamReader = New StreamReader(filePath)
            ' Read one line from file
            Dim InLine As String = reader.ReadLine


            ' Loop over each line in file, While list is Not Nothing.
            Do While (Not InLine Is Nothing)

                LineText = InLine

                Dim EmailCode As String = ""
                Dim Ptr As Integer = InStr(LCase$(InLine), "<report")
                If Ptr > 0 Then
                    InLine = Right$(InLine, InLine.Length - Ptr)
                    '! Unicode function is faster: AscW
                    While Asc(InLine) <> 62      '>
                        EmailCode = EmailCode + Left$(InLine, 1)
                        InLine = Right$(InLine, InLine.Length - 1)
                        If Len(InLine) = 0 Then     'PWR 09.09.2019 Code to exit out if there is no ">" on the line.
                            If WarningMessage_Ask("Report command is not valid" & vbNewLine & vbNewLine & EmailCode & vbNewLine & vbNewLine &
                                    "Please Check and fix above, most likely there is a typo in the database, and then Retry", "Report Command is not valid") = vbYes Then
                            End If
                            Exit While
                        End If
                    End While
                    If InStr(LCase$(EmailCode), "reference=") Then
                        CurrentReportRef = GetQuote("reference=", EmailCode)
                        CleanReportRef(CurrentReportRef)
                        '        If ReportIndex = 0 Then                'PWR 17.10.2004 CHanged from -1 to 0
                        If ReportIndex = -1 Then                'PWR 17.10.2004 CHanged from -1 to 0
                            Call NewReport()
                        ElseIf Report(ReportIndex).FileReference <> CurrentReportRef Then
                            Call NewReport()
                        End If
                        Report(ReportIndex).FileReference = CurrentReportRef
                        'Call Create_ReportFilename
                        '! Too many uncommented lines: 30
                    End If
                    If InStr(LCase$(EmailCode), "owners=") Then
                        Report(ReportIndex).FileOwners = GetQuote("owners=", EmailCode)
                    End If
                    'If InStr(LCase$(EmailCode), "printer=") Then
                    '  PrinterTest = LCase$(GetQuote("printer=", EmailCode))
                    '  For x = 0 To Form1.Combo1.ListCount
                    '      If InStr(1, LCase$(Form1.Combo1.List(x)), PrinterTest) > 0 Then
                    '          Form1.Combo1.Text = Form1.Combo1.List(x)
                    '          Report(ReportIndex).Printer = PrinterTest          'PWR 29.05.2015 Store the Printer Tray Per Report Page.
                    '          Call Change_Printers()
                    '          Exit For
                    '      End If
                    '  Next
                    'End If
                    'If InStr(LCase$(EmailCode), "tray=") Then
                    ' PrinterTest = LCase$(GetQuote("tray=", EmailCode))
                    ' For x = 0 To Form1.Combo2.ListCount
                    ' If InStr(1, LCase$(Form1.Combo2.List(x)), PrinterTest) > 0 Then
                    ' Form1.Combo2.Text = Form1.Combo2.List(x)
                    ' Report(ReportIndex).Tray = PrinterTest          'PWR 29.05.2015 Store the Printer Tray Per Report Page.
                    ' gCurrentTray = PrinterTest
                    ' SelectedBin = x
                    ' If (Form1.Combo2.List(x).length = PrinterTest.length) Then         'When the Tray Name contains the Tray Name of another longer named Tray.
                    ' Exit For
                    'End If
                    'End If
                    'Next
                    'End If
                    'If InStr(LCase$(EmailCode), "fax=") Then
                    'Report(ReportIndex).FaxNumber = GetQuote("fax=", EmailCode)
                    'Ptr = 1
                    'TestLen = Report(ReportIndex).FaxNumber.Length
                    'While Ptr < TestLen
                    '  TestChr = CStr(Mid$(Report(ReportIndex).FaxNumber, Ptr, 1))
                    '   If InStr(1, "0123456789", CStr(TestChr)) = 0 Then 'If Data is not a Number
                    '        Report(ReportIndex).FaxNumber = Replace(Report(ReportIndex).FaxNumber, TestChr, "")
                    '     End If
                    '      Ptr = Ptr + 1
                    '   End While
                    'End If
                    If InStr(LCase$(EmailCode), "subject=") Then
                        Report(ReportIndex).EmailSubject = GetQuote("subject=", EmailCode)
                    End If
                    'If InStr(LCase$(EmailCode), "filetype=") Then
                    'AttachmentType = LCase$(GetQuote("filetype=", EmailCode))
                    'If LCase$(AttachmentType) = "non" Then          'PWR 09.07.2015 Changes to
                    '   Form5.AttachFileTick.Value = 0
                    '   Form5.Frame_Attach.Visible = False
                    ''Else
                    '    Form5.AttachFileTick.Value = 1
                    '    Form5.Frame_Attach.Visible = True
                    'End If
                    'End If
                    If InStr(LCase$(EmailCode), "body=") Then
                        Report(ReportIndex).EmailBody = Report(ReportIndex).EmailBody & GetQuote("body=", EmailCode)
                    End If

                    If InStr(LCase$(EmailCode), "from=") Then       'PWR 27.01.2017
                        Report(ReportIndex).EmailFrom = GetQuote("from=", EmailCode)
                    End If

                    If InStr(LCase$(EmailCode), "replyto=") Then       'PWR 27.01.2017
                        Report(ReportIndex).EmailReplyTo = GetQuote("replyto=", EmailCode)
                    End If
                End If

                InLine = LineText     'PWR 26.08.2015 Reset InLine Now to full String

                Dim LastPtr As Integer = 0

                EmailCode = vbNullString                                'Get Email Address Information
                Ptr = InStr(LCase$(InLine), "<email")

                While (Ptr > 0)
                    LastPtr = Ptr
                    'On Error GoTo EmailError
                    InLine = Right$(InLine, InLine.Length - Ptr)
                    '! Unicode function is faster: AscW
                    While Asc(InLine) <> 62      '>
                        EmailCode = EmailCode + Left$(InLine, 1)
                        InLine = Right$(InLine, InLine.Length - 1)
                        If Len(InLine) = 0 Then     'PWR 09.09.2019 Code to exit out if there is no ">" on the line.
                            If WarningMessage_Ask("Email command is not valid" & vbNewLine & vbNewLine & EmailCode & vbNewLine & vbNewLine &
                                    "Please Check and fix above, most likely there is a typo in the database, and then Retry", "Email Command is not valid") = vbYes Then
                            End If
                            Exit While
                        End If
                    End While
                    If InStr(LCase$(EmailCode), "address=") Then
                        Dim xEmailAddress As String
                        xEmailAddress = GetQuote("address=", EmailCode)
                        If InStr(1, xEmailAddress, "@") <> 0 Then   'PWR 20.04.2010 Make sure there is an Email Address in the value.
                            If ReportIndex = -1 Then
                                Call NewReport()
                            ElseIf Report(ReportIndex).FileReference <> CurrentReportRef Then
                                Call NewReport()
                            End If
                            EmailIndex = EmailIndex + 1
                            Report(ReportIndex).EmailIndexCount = EmailIndex
                            Report(ReportIndex).Email(EmailIndex).EmailReceipt = xEmailAddress
                            Report(ReportIndex).Email(EmailIndex).EmailFullName = ""
                            Report(ReportIndex).Email(EmailIndex).EmailSend = 0
                            If EmailIndex = 0 Then
                                Report(ReportIndex).MainEmailAddress = Report(ReportIndex).Email(EmailIndex).EmailReceipt
                            End If
                        Else
                            If WarningMessage_Ask("Email Address is not valid" & vbNewLine & vbNewLine & xEmailAddress & vbNewLine & vbNewLine & EmailCode & vbNewLine & vbNewLine &
                                    "Please Fix and Retry", "Email Address is not valid") = vbYes Then

                            End If
                        End If
                    Else
                        If InStr(LCase$(EmailCode), "subject=") Then        'PWR 26.11.2007 If there is no Address in the Email Line, just a Subject
                            '! Too many uncommented lines: 31
                            If ReportIndex = -1 Then
                                Call NewReport()
                            ElseIf Report(ReportIndex).FileReference <> CurrentReportRef Then
                                Call NewReport()
                            End If
                            'EmailIndex = EmailIndex + 1             'PWR 30.01.2017 THis is not needed if there are no Email addresses
                            'Report(ReportIndex).EmailIndexCount = EmailIndex
                        End If
                    End If

                    'If ReportIndex > -1 And EmailIndex > -1 Then        'PWR 20.04.2010 Make sure there is a Report Index.
                    If ReportIndex > -1 Then        'PWR 20.04.2010 Make sure there is a Report Index.
                        If EmailIndex > -1 Then     'PWR 22.11.2013 Only need to do if there is a Report Index
                            'PWR 05.10.2010 Remove this Function, as it was trying to lookup this Contact Name in Outlook.
                            If InStr(LCase$(EmailCode), "name=") Then
                                Report(ReportIndex).Email(EmailIndex).EmailFullName = GetQuote("name=", EmailCode)
                            End If

                            If LCase$(GetQuote("send=", EmailCode)) = "to" Then
                                Report(ReportIndex).Email(EmailIndex).EmailSend = 1
                            ElseIf LCase$(GetQuote("send=", EmailCode)) = "cc" Then
                                Report(ReportIndex).Email(EmailIndex).EmailSend = 2
                            ElseIf LCase$(GetQuote("send=", EmailCode)) = "bcc" Then
                                Report(ReportIndex).Email(EmailIndex).EmailSend = 3
                            ElseIf LCase$(GetQuote("send=", EmailCode)) = "from" Then
                                Report(ReportIndex).EmailFrom = GetQuote("address=", EmailCode)
                            End If
                        End If

                        'If InStr(LCase$(EmailCode), "filetype=") Then
                        '    AttachmentType = LCase$(GetQuote("filetype=", EmailCode))
                        'End If

                        'If InStr(LCase$(EmailCode), "fax=") Then
                        '    Report(ReportIndex).FaxNumber = GetQuote("fax=", EmailCode)
                        'End If
                        If InStr(LCase$(EmailCode), "subject=") Then
                            Report(ReportIndex).EmailSubject = GetQuote("subject=", EmailCode)
                        End If
                        If InStr(LCase$(EmailCode), "body=") Then       'Should be adding to Body regardless if there is a EmailIndex
                            Report(ReportIndex).EmailBody = Report(ReportIndex).EmailBody & GetQuote("body=", EmailCode)
                        End If

                        If InStr(LCase$(EmailCode), "from=") Then
                            Report(ReportIndex).EmailFrom = GetQuote("from=", EmailCode)
                        End If

                        If InStr(LCase$(EmailCode), "replyto=") Then
                            Report(ReportIndex).EmailReplyTo = GetQuote("replyto=", EmailCode)
                        End If

                    End If

                    Ptr = (LastPtr + 1)
                    InLine = LineText     'PWR 26.08.2015 Reset InLine Now to full String
                    EmailCode = vbNullString                                'Get Email Address Information
                    Ptr = InStr((LastPtr + 1), LCase$(InLine), "<email")
                End While

                InLine = LineText     'PWR 26.08.2015 Reset InLine Now to full String

                EmailCode = vbNullString                                'Get Attached File Information
                Ptr = InStr(LCase$(InLine), "<attach")
                While Ptr > 0                   'Changed to While for Multiple Attachements
                    LastPtr = Ptr
                    InLine = Right$(InLine, InLine.Length - Ptr)
                    '! Unicode function is faster: AscW
                    While Asc(InLine) <> 62      '>
                        EmailCode = EmailCode + Left$(InLine, 1)
                        InLine = Right$(InLine, InLine.Length - 1)
                        If Len(InLine) = 0 Then     'PWR 09.09.2019 Code to exit out if there is no ">" on the line.
                            If WarningMessage_Ask("Attach command is not valid" & vbNewLine & vbNewLine & EmailCode & vbNewLine & vbNewLine &
                                    "Please Check and fix above, most likely there is a typo in the database, and then Retry", "Attach Command is not valid") = vbYes Then
                            End If
                            Exit While
                        End If
                    End While

                    If InStr(LCase$(EmailCode), "file=") Then
                        If ReportIndex = -1 Then
                            Call NewReport()
                        ElseIf Report(ReportIndex).FileReference <> CurrentReportRef Then
                            Call NewReport()
                        End If
                        AttachIndex = AttachIndex + 1
                        Report(ReportIndex).AttachIndexCount = AttachIndex
                        Report(ReportIndex).Attach(AttachIndex).File = GetQuote("file=", EmailCode)
                        Report(ReportIndex).Attach(AttachIndex).Directory = ""
                        Report(ReportIndex).Attach(AttachIndex).Name = ""
                        Report(ReportIndex).Attach(AttachIndex).SecureReport = False

                        'If AttachIndex = 0 Then
                        '    Report(ReportIndex).MainAttachFile = Report(ReportIndex).Attach(AttachIndex).File
                        'End If
                    End If


                    If InStr(LCase$(EmailCode), "securereport=") Then               'PWR 30.07.2018 - If it is a Secure Report then the Details of the PDF needs to be deleted after it has been sent.
                        If LCase(GetQuote("securereport=", EmailCode)) = "true" Then
                            Report(ReportIndex).Attach(AttachIndex).SecureReport = True
                        Else
                            Report(ReportIndex).Attach(AttachIndex).SecureReport = False
                        End If
                    End If

                    If InStr(LCase$(EmailCode), "dir=") Then
                        Report(ReportIndex).Attach(AttachIndex).Directory = GetQuote("dir=", EmailCode)
                    End If

                    'PWR 05.10.2010 Remove this Function, as it was trying to lookup this Contact Name in Outlook.
                    'If InStr(LCase$(EmailCode), "name=") Then
                    '    Report(ReportIndex).Attach(AttachIndex).Name = GetQuote("name=", EmailCode)
                    'End If
                    If Report(ReportIndex).Attach(AttachIndex).Directory.Length = 0 Then
                        If SelectedReportFolder = "" Then
                            Report(ReportIndex).Attach(AttachIndex).Directory = RptFilePath
                        Else
                            Report(ReportIndex).Attach(AttachIndex).Directory = SelectedReportFolder
                        End If

                    End If

                    If Report(ReportIndex).Attach(AttachIndex).Name.Length = 0 Then
                        Report(ReportIndex).Attach(AttachIndex).Name = Report(ReportIndex).Attach(AttachIndex).File
                        Dim nPos As Integer = InStrRev(Report(ReportIndex).Attach(AttachIndex).Name, "\")
                        If (nPos = 0) Then
                            nPos = InStrRev(Report(ReportIndex).Attach(AttachIndex).Name, "/")
                        End If
                        If nPos > 0 Then
                            Report(ReportIndex).Attach(AttachIndex).Name = Mid$(Report(ReportIndex).Attach(AttachIndex).Name, nPos + 1, 100)
                        End If
                    End If


                    If Right$(Report(ReportIndex).Attach(AttachIndex).Directory, 1) <> "\" Then Report(ReportIndex).Attach(AttachIndex).Directory =
                           Report(ReportIndex).Attach(AttachIndex).Directory & "\"

                    AttachPath = SetFilePath(Report(ReportIndex).Attach(AttachIndex).Directory, Report(ReportIndex).Attach(AttachIndex).File)
                    If Not CheckDocumentExists(AttachPath) Then



                        'PWR 14.09.2012 Search if Is not related to Database Directory
                        If Right$(Report(ReportIndex).Attach(AttachIndex).Directory, 1) = "\" Then
                            Report(ReportIndex).Attach(AttachIndex).Directory = Report(ReportIndex).Attach(AttachIndex).Directory & "Docs\"
                        Else
                            Report(ReportIndex).Attach(AttachIndex).Directory = Report(ReportIndex).Attach(AttachIndex).Directory & "\Docs\"
                        End If
                        AttachPath = SetFilePath(Report(ReportIndex).Attach(AttachIndex).Directory, Report(ReportIndex).Attach(AttachIndex).File)
                        If Not CheckDocumentExists(AttachPath) Then
                            '08.06.2011 Revert Back to Blank if the File Found Doesn't exist.
                            If ShowAttachFileErrors Then
                                'If MsgBox("Attached File to Report does not exist" & vbNewLine & vbNewLine & AttachPath & vbNewLine & vbNewLine & _
                                '"Do you want to Continue to see other Error Messages?", vbYesNo, "Attached File Does not Exist.") = vbYes Then
                                If WarningMessage_Ask("Attached File to Report does not exist" & vbNewLine & vbNewLine & AttachPath & vbNewLine & vbNewLine &
                                         "Report File Path : " & RptFilePath & vbNewLine & vbNewLine &
                                    "Do you want to Continue to see other Error Messages?", "Attached File Does not Exist.") = vbYes Then
                                    ShowAttachFileErrors = True
                                Else
                                    ShowAttachFileErrors = False
                                End If
                            End If
                            Report(ReportIndex).Attach(AttachIndex).File = vbNullString
                            Report(ReportIndex).Attach(AttachIndex).Name = vbNullString
                            Report(ReportIndex).Attach(AttachIndex).Directory = vbNullString
                            AttachIndex = AttachIndex - 1
                            If Report(ReportIndex).AttachIndexCount > 0 Then
                                Report(ReportIndex).AttachIndexCount = AttachIndex
                            End If
                        End If
                    End If

                    Ptr = (LastPtr + 1)
                    InLine = LineText     'PWR 26.08.2015 Reset InLine Now to full String
                    EmailCode = vbNullString                                'Get Email Address Information
                    Ptr = InStr((LastPtr + 1), LCase$(InLine), "<attach")
                End While

                'Get the next line
                LineText = ""
                InLine = reader.ReadLine

            Loop

        End Using


        'MsgBox("Read Report File Name :" + ReportFileName + Environment.NewLine +
        '       "Path :" + filePath + Environment.NewLine, MessageBoxButtons.OK, "IASendEmail - Report DataChecked ")

    End Sub

End Module
