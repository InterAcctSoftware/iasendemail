﻿Imports System.ComponentModel
Imports System
Imports System.Threading
Imports System.IO


Public Class Form1
    Private bInstalled As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Async Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Text = ApplicationTitle

        'PWR 13.11.2018 New Process to test if the program is already running.
        'Dim firstInstance As Boolean = False
        'If Process.GetProcessesByName _
        '  (Process.GetCurrentProcess.ProcessName).Length > 1 Then

        '    Dim timeCounter As Integer = 0
        '    While (Not firstInstance) And (timeCounter < 10)
        '        'AddtoErrorLog("Form1", "Load", "Program is already running, I will wait (30 seconds) till it is ready to process", "Program is already Running.")
        '        Thread.Sleep(30000)         'Sleep for 10 seconds
        '        If Process.GetProcessesByName _
        '        (Process.GetCurrentProcess.ProcessName).Length > 1 Then
        '            firstInstance = False
        '        Else
        '            firstInstance = True
        '        End If
        '        timeCounter += 1
        '    End While
        '    If timeCounter = 10 Then
        '        AddtoErrorLog("Form1", "Load", "Program Waited for Too Line, and Exited", "Program Exited")
        '        Application.Exit()
        '    End If

        'End If


        Try
            Dim oApp As Object = CreateObject("Outlook.Application")
            bInstalled = True
        Catch
            bInstalled = False
            'MessageBox.Show("Office Application not installed!", "Office Installed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

        If My.Settings.EmailMethod < 0 Then   'This is the first time the program has loaded for this user.. decide with Email method will be used, and if it is SMTP then ask for the Server/Username
            Dim MsgBoxResult As Integer = -1
            If bInstalled Then
                My.Settings.OutlookAvailable = True
                MsgBoxResult = MsgBox("There are 4 options to use for emailing:" & vbNewLine & "1. Outlook Client (Default)" & vbNewLine & "2. Office 365" & vbNewLine & "3. Outlook via TPDuet" & vbNewLine & "4. SMTP" & vbNewLine & vbNewLine & "Do you want to use Outlook client for sending emails?", vbYesNo, "Initial Mail Setup - Select Email Method")
                If MsgBoxResult = vbNo Then
                    My.Settings.EmailMethod = 1     'SMTP
                Else
                    My.Settings.EmailMethod = 0     'Outlook
                End If
            Else        'If Outlook is not available
                'MsgBoxResult = MsgBox("Outlook is not Available for Emailing. " & vbNewLine & vbNewLine & "Please Setup SMTP Settings", MsgBoxStyle.OkOnly, "SMTP Email Method")
                My.Settings.EmailMethod = 1
            End If

            If My.Settings.EmailMethod = 1 Then
                MsgBoxResult = MsgBox("Do you want to use Office 365 (MS Graph) for sending emails? ", vbYesNo, "Select Email Method - Office 365 (MS Graph)")
                If MsgBoxResult = vbNo Then
                    My.Settings.EmailMethod = 1     'SMTP
                Else
                    My.Settings.EmailMethod = 5     'Office 365 (MS Graph)
                End If
            End If

            If My.Settings.EmailMethod = 1 Then
                MsgBoxResult = MsgBox("Do you want to use Office 365 (Web Mail) for sending emails? ", vbYesNo, "Select Email Method - Office 365 (Web Mail)")
                If MsgBoxResult = vbNo Then
                    My.Settings.EmailMethod = 1     'SMTP
                Else
                    My.Settings.EmailMethod = 2     'Office 365 (WebMail)
                End If
            End If

            If My.Settings.EmailMethod = 1 Then
                MsgBoxResult = MsgBox("Do you want to use EWS (Exchange Web Services) for sending emails? ", vbYesNo, "Select Email Method - EWS")
                If MsgBoxResult = vbNo Then
                    My.Settings.EmailMethod = 1     'SMTP
                Else
                    My.Settings.EmailMethod = 3     'EWS
                End If
            End If

            If My.Settings.EmailMethod = 1 Then
                MsgBoxResult = MsgBox("Do you want to use Outlook via TPDuet with NetLeverage for sending emails? ", vbYesNo, "Select Email Method - Outlook via TPDuet")
                If MsgBoxResult = vbNo Then
                    My.Settings.EmailMethod = 1     'SMTP
                Else
                    My.Settings.EmailMethod = 4     'Office via NetLeverage
                End If
            End If

            My.Settings.Save()
        End If

        If My.Settings.SMTPEmail.Length > 0 Then
            Me.txtSMTP_Email.Text = My.Settings.SMTPEmail
        End If

        If My.Settings.EmailMethod = 2 Then
            Me.txtSMTP_Server.Text = "https://outlook.office365.com/ews/exchange.asmx"
        Else
            If My.Settings.EmailMethod = 5 Then
                Me.txtSMTP_Server.Text = "https://graph.microsoft.com/v1.0/"
            Else
                If My.Settings.SMTPServer.Length > 0 Then
                    Me.txtSMTP_Server.Text = My.Settings.SMTPServer
                End If
            End If
        End If


        If My.Settings.Password.Length > 0 Then
            'Me.txtPassword.Text = base64Decode(My.Settings.Password)
            Me.txtPassword.Text = Decrypt(My.Settings.Password)
        End If

        'PWR 29.10.2024 Added so we use teh correct Port for SMTP
        Me.CheckBoxSSL.Checked = My.Settings.SMTPSSL
        Me.TextBoxPort.Text = My.Settings.SMTPPort

        If ((My.Settings.EmailMethod = 2) Or (My.Settings.EmailMethod = 5)) Then
            'Use Office 365

        ElseIf bInstalled = False Or My.Settings.EmailMethod = 1 Then
            If (Len(My.Settings.SMTPServer) <> 0) And (Len(My.Settings.SMTPEmail) <> 0) Then        'PWR 25.11.2016 Check if SMTP is valid too.
                Me.EmailMethod_SMTP.Checked = True
                If bInstalled = False And My.Settings.OutlookAvailable = False Then 'PWR 25.11.2016 Not able to use Outlook at this time.
                    Me.EmailMethod_Outlook.Enabled = False
                End If
            Else
                If My.Settings.OutlookAvailable = True Then
                    'Try to open Outlook First
                    'Call Open_Outlook()
                    Me.txtSMTP_Email.Text = Me.txtSMTP_Email.Text

                End If
            End If
        End If

        'Me.EmailMethod_Outlook.SelectedIndex = My.Settings.EmailMethod

        Select Case My.Settings.EmailMethod
            Case 0 : Me.EmailMethod_Outlook.Checked = True

            Case 2 : Me.EmailMethod_Office365.Checked = True
            Case 3 : Me.EmailMethod_EWS.Checked = True
            Case 4 : Me.EmailMethod_TPDuet.Checked = True
            Case 5 : Me.EmailMethod_MSGraph.Checked = True
            Case Else : Me.EmailMethod_SMTP.Checked = True

        End Select

        Call Show_SMTPFields()
        Call Show_SendButton()


        Dim Commands() As String = Environment.GetCommandLineArgs()

        If Commands.Length > 1 Then   'Report to run
            Load_Program()
            Await BulkSendPrint()
            If (Not ErrorMessageExists) Then
                End
            End If
        Else
            Open_Report_File()
        End If

        'MsgBox("Finished Loading Report", MessageBoxButtons.OK, "IASendEmail - Finished Loading")

        'AddtoErrorLog("Form1", "Load", "Ex.Messsage", "Full Description")      'PWR 03.10.2018 Only for testing.

    End Sub

    Private Sub EmailMethod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailMethod_Outlook.CheckedChanged
        'Show/Hide the SMTP Fields based on the Type
        'My.Settings.EmailMethod = EmailMethod.SelectedIndex
        'My.Settings.Save()

        Show_SMTPFields()
        Show_SendButton()

    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        SaveSettings()
        SaveToDraft = False
        Load_Program()
        BulkSendPrint()
    End Sub



    Private Sub txtSMTP_Server_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTP_Server.TextChanged
        Call Show_SendButton()
        ErrorForceStop = False
    End Sub

    Private Sub txtSMTP_Email_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTP_Email.TextChanged
        Call Show_SendButton()
        ErrorForceStop = False
    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        Dim strm As System.IO.Stream
        strm = OpenFileDialog1.OpenFile()

        ReportFileName = OpenFileDialog1.FileName.ToString()

        SelectedReportFolder = Path.GetDirectoryName(ReportFileName)

        If Not (strm Is Nothing) Then

            'insert code to read the file data

            strm.Close()

            'MessageBox.Show("file closed")

        Else
            ReportFileName = ""
        End If
    End Sub

    Private Sub btnDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDraft.Click
        SaveSettings()
        SaveToDraft = True
        Load_Program()
        BulkSendPrint()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
        End
    End Sub

    Private Sub btnDraftOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnDraftOpen.Click
        SaveSettings()
        SaveToDraft = True
        OpenEmailForm = True
        Load_Program()
        BulkSendPrint()
    End Sub

    Private Sub txtPassword_EditValueChanged(sender As Object, e As EventArgs) Handles txtPassword.TextChanged
        Call Show_SendButton()
        ErrorForceStop = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        SaveSettings()
        Close()
        End
    End Sub

    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        If Me.txtPassword.PasswordChar = "*" Then
            Me.txtPassword.PasswordChar = ""
            Me.btnShow.Text = "Hide Password"
        Else
            Me.txtPassword.PasswordChar = "*"
            Me.btnShow.Text = "Show Password"
        End If
    End Sub

    Private Sub EmailMethod_MSGraph_CheckedChanged(sender As Object, e As EventArgs) Handles EmailMethod_MSGraph.CheckedChanged
        Show_SMTPFields()
        Show_SendButton()
    End Sub

    Private Sub EmailMethod_SMTP_CheckedChanged(sender As Object, e As EventArgs) Handles EmailMethod_SMTP.CheckedChanged
        Show_SMTPFields()
        Show_SendButton()
    End Sub

    Private Sub EmailMethod_EWS_CheckedChanged(sender As Object, e As EventArgs) Handles EmailMethod_EWS.CheckedChanged
        Show_SMTPFields()
        Show_SendButton()
    End Sub

    Private Sub EmailMethod_Office365_CheckedChanged(sender As Object, e As EventArgs) Handles EmailMethod_Office365.CheckedChanged
        Show_SMTPFields()
        Show_SendButton()
    End Sub

    Private Sub EmailMethod_TPDuet_CheckedChanged(sender As Object, e As EventArgs) Handles EmailMethod_TPDuet.CheckedChanged
        Show_SMTPFields()
        Show_SendButton()
    End Sub
End Class

