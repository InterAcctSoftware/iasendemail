﻿Module ErrorLog

    Public Sub AddtoErrorLog(CalledbyRoutine As String, CalledbyFunction As String, ByRef ErrorMessage As String, ErrorDescription As String, Optional EmailToString As String = "", Optional EmailFrom As String = "", Optional EmailSubject As String = "", Optional ReTryCount As Integer = 0)
        'PWR 03.10.2018 New Routine to create a CSV File for the Error messages.

        Dim ErrorLogFileName As String = My.Application.Info.DirectoryPath
        If Mid(ErrorLogFileName, Len(ErrorLogFileName), 1) <> "\" Then
            ErrorLogFileName = ErrorLogFileName & "\Errors\IASendEmail\"
        Else
            ErrorLogFileName = ErrorLogFileName & "Errors\IASendEmail\"
        End If

        System.IO.Directory.CreateDirectory(ErrorLogFileName)
        ErrorLogFileName = ErrorLogFileName & "IASendEmail_Errors_" & Format(Today, "yyyyMMdd") & ".csv"

        Dim Seperator As String = ChrW(34) & "," & ChrW(34)

        Dim swauto As IO.StreamWriter
        If Not (IO.File.Exists(ErrorLogFileName)) Then
            swauto = IO.File.CreateText(ErrorLogFileName)
            swauto.WriteLine(String.Format("{0}Date{1}Time{1}Report{1}Windows User{1}Email From{1}Email To{1}Subject{1}Routine{1}Function{1}Error Description{1}Error Message{1}Version{1}ReTry Counter{0}", ChrW(34), Seperator))
        Else
            swauto = IO.File.AppendText(ErrorLogFileName)
        End If
        Dim xPos As Integer = InStrRev(ReportFileName, "\")

        Dim userEmailAddress As String = My.Settings.SMTPEmail
        If (EmailFrom.Length > 0) Then
            userEmailAddress = EmailFrom
        Else
            userEmailAddress = "<Logged in Account>"
        End If

        If Not ErrorMessageExists Then
            ErrorMessageText = String.Join(Environment.NewLine, ErrorDescription, "", "Email From : " + userEmailAddress, "Email To : " + EmailToString, "Subject : " + EmailSubject, "", "Error Reply:", ErrorMessage)
            ErrorMessageExists = True

            If ErrorMessage.Contains("401") Then
                ErrorMessageText = ErrorMessageText + Environment.NewLine + Environment.NewLine + "Check and Fix Username and Password used."
                ErrorForceStop = True
            End If


        End If



        Dim JustRPTFileName As String = Mid(ReportFileName, (xPos + 1))

        Dim CurrentTime As DateTime = DateTime.Now
        swauto.WriteLine(String.Format("{0}{2}{1}{3}{1}{4}{1}{5}{1}{6}{1}{7}{1}{8}{1}{9}{1}{10}{1}{11}{1}{12}{1}{13}{0}", ChrW(34), Seperator, Format(Today, "dd/MM/yyyy"), CurrentTime.ToString("H:mm:ss"), JustRPTFileName, Environment.UserName, userEmailAddress, EmailToString, EmailSubject, CalledbyRoutine, CalledbyFunction, ErrorDescription, ErrorMessage, Application.ProductVersion, ReTryCount))
        swauto.Close()


    End Sub



End Module
