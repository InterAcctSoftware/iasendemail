﻿Imports System.IO
Imports System.Text
Imports System.Net
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.Net.Mail
Imports System.Runtime.InteropServices
Imports Microsoft.Exchange.WebServices.Data
Imports Microsoft.Exchange.WebServices.Autodiscover
Imports Microsoft.Identity
'Imports Microsoft.Graph


Module SendEmail

    Public ErrorMessageExists As Boolean = False
    Public ErrorMessageText As String = ""
    Public ErrorForceStop = False

    Dim ReTryCount As Integer = 0
    Dim EmailBodyHTML As String = ""
    Dim EmailBodyPlain As String = ""

    Public Enum Methods
        Outlook = 0
        SMTP = 1
        Office365 = 2
        EWS = 3
        TPDuet = 4
        MSGraph = 5
    End Enum

    Private Function AddRecipientToMailItem(ByVal mail As Outlook._MailItem,
      ByVal fullName As String,
      ByVal recipientType As Outlook.OlMailRecipientType) As Outlook.Recipient
        Dim recipients As Outlook.Recipients = TryCast(mail.Recipients, Outlook.Recipients)
        If recipients IsNot Nothing Then
            Try
                Dim recipient As Outlook.Recipient = TryCast(recipients.Add(fullName), Outlook.Recipient)
                If recipient IsNot Nothing Then
                    recipient.Resolve()
                    If recipient.Resolved Then
                        recipient.Type = CInt(recipientType)
                        recipients.ResolveAll()
                        Return recipient
                    Else
                        recipient.Delete()
                        Return Nothing
                    End If
                End If
                Return Nothing
            Finally
                Marshal.ReleaseComObject(recipients)
            End Try
        Else
            Return Nothing
        End If
    End Function

    Function RedirectionCallback(url As String) As Boolean
        Return url.ToLower().StartsWith("https://")
    End Function

    Public Sub SaveSettings()

        My.Settings.EmailMethod = Find_Current_EmailMethod()


        If (My.Settings.EmailMethod <> Methods.MSGraph) And (My.Settings.EmailMethod <> Methods.Office365) Then
            My.Settings.SMTPServer = Form1.txtSMTP_Server.Text
        Else
            My.Settings.SMTPServer = ""
        End If
        My.Settings.SMTPEmail = Form1.txtSMTP_Email.Text
        My.Settings.Password = Encrypt(Form1.txtPassword.Text)

        My.Settings.SMTPPort = Form1.TextBoxPort.Text
        My.Settings.SMTPSSL = Form1.CheckBoxSSL.Checked
        My.Settings.Save()
    End Sub
    Public Sub Send_By_EWS(ByVal xReportIndex As Integer, ByVal Office365Link As Boolean)

        Dim ReadytoEmail As Boolean = False
        Dim SendToString As String = ""

        Dim pass As String = Form1.txtPassword.Text
        Dim userEmailAddress As String = My.Settings.SMTPEmail
        'Dim Office365WebserivceURL As String = My.Settings.SMTPServer
        Dim Office365WebserivceURL As String = Form1.txtSMTP_Server.Text

        'If pass Is Nothing Then
        '    pass = "s8-@S#A,4[?Y"
        'End If

        'If userEmailAddress Is Nothing Then
        '    userEmailAddress = "paul@interacct.com.au"
        'End If

        If Office365WebserivceURL Is Nothing Then
            If Office365Link Then
                Office365WebserivceURL = "https://outlook.office365.com/ews/exchange.asmx"
            Else
                'Office365WebserivceURL = "https://outlook.office365.com/ews/exchange.asmx"
                Form1.txtResult.Text = "No EWS Server Setup"
                Exit Sub
            End If
        End If

        'If pass.Length = 0 Then
        '    pass = "s8-@S#A,4[?Y"

        'End If

        'If userEmailAddress.Length = 0 Then
        '    userEmailAddress = "paul@interacct.com.au"
        'End If

        If Office365WebserivceURL.Length = 0 Then
            'Office365WebserivceURL = "https://outlook.office.com/api"
            If Office365Link Then
                Office365WebserivceURL = "https://outlook.office365.com/ews/exchange.asmx"
            Else
                'Office365WebserivceURL = "https://outlook.office365.com/ews/exchange.asmx"
                Form1.txtResult.Text = "No EWS Server Setup"
                Exit Sub
            End If
        End If

        Dim myservice As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP1) With {
            .Credentials = New WebCredentials(userEmailAddress, pass)
        }



        Try


            Dim serviceUrl As String = Office365WebserivceURL
            myservice.Url = New Uri(serviceUrl)


            'ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls

            If My.Settings.SMTPSSL Then
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                'ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault
            Else
                ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls12 Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls)
            End If


            'Test Email
            'emailMessage.Subject = "Test Subject"
            'emailMessage.Body = New MessageBody("Testing Exchange Web Service API")
            'emailMessage.ToRecipients.Add(recipients)
            'emailMessage.Send()
            Dim emailMessage As EmailMessage = New EmailMessage(myservice) With {
                .Subject = Report(xReportIndex).EmailSubject
            }
            If (Report(xReportIndex).EmailFrom.Length > 0) Then
                emailMessage.From = Report(xReportIndex).EmailFrom
            End If




            'Looking at Attachments
            Dim xAttachedFileName As String = vbNullString
            Try
                If Report(xReportIndex).AttachIndexCount >= 0 Then   'PWR 25.11.2016 Make sure there are attachments.

                    Dim ExtraHTMLFiles As Boolean = False


                    For x As Integer = 0 To Report(xReportIndex).AttachIndexCount

                        If Not IsNothing(Report(xReportIndex).Attach(x).File) Then
                            If Report(xReportIndex).Attach(x).File.Length > 0 Then
                                If (InStr(LCase(Report(xReportIndex).Attach(x).File), ".htm") = 0) Or (ExtraHTMLFiles = True) Then   'Don't include the Base HTML File.

                                    xAttachedFileName = vbNullString

                                    If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                        xAttachedFileName = Report(xReportIndex).Attach(x).File
                                    Else
                                        xAttachedFileName = (Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File)
                                    End If

                                    If File.Exists(xAttachedFileName) Then          'PWR 07.12.2016 Check if the File exists first before adding.
                                        Try
                                            emailMessage.Attachments.AddFileAttachment(xAttachedFileName)
                                        Catch ex As Exception
                                            'System.Windows.Forms.MessageBox.Show("Error when trying to add attachements " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Office365")
                                            AddtoErrorLog("SendEmail", "Send_By_Office365 myAttachments.Add", ex.Message, "Error when trying to add attachements " & xAttachedFileName, "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
                                        Finally
                                            ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
                                        End Try
                                    End If

                                Else
                                    ExtraHTMLFiles = True
                                End If
                            Else
                                Exit For
                            End If
                        Else
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when trying to add attachements " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Office365")
                AddtoErrorLog("SendEmail", "Send_By_Office365", ex.Message, "Error when trying to add attachements " & xAttachedFileName, "", userEmailAddress, Report(xReportIndex).EmailSubject)
            Finally
                ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
            End Try



            For x As Integer = 0 To Report(xReportIndex).EmailIndexCount
                If Not IsNothing(Report(xReportIndex).Email(x).EmailReceipt) Then   'Find if there is an Email Address

                    Select Case Report(xReportIndex).Email(x).EmailSend 'To Send as 1.TO or 2.CC or 3.BCC
                        Case 2
                            emailMessage.CcRecipients.Add(Report(xReportIndex).Email(x).EmailReceipt)
                        Case 3
                            emailMessage.BccRecipients.Add(Report(xReportIndex).Email(x).EmailReceipt)
                        Case 4
                        Case Else
                            emailMessage.ToRecipients.Add(Report(xReportIndex).Email(x).EmailReceipt)
                    End Select
                    'retValue = Recipients.ResolveAll()
                    SendToString = SendToString & Report(xReportIndex).Email(x).EmailReceipt & " "

                    ReadytoEmail = True
                Else
                    If x = 0 Then       'PWR 10.12.2018 If X is zero only.
                        OpenEmailForm = True
                    End If
                    Exit For
                End If
            Next

            SendToString = Trim(SendToString)

            emailMessage.Body = Replace(EmailBodyPlain, "|", vbNewLine)      'Plain Text Body

            If EmailBodyHTML = "" Then
                emailMessage.Body.BodyType = 1      'Text
            Else
                emailMessage.Body.BodyType = 0      'HTML
            End If

            SaveSettings()      'Save Settings for next time.

            If ReadytoEmail And SaveToDraft = False Then
                'MsgBox("Sending Now" & Environment.NewLine &
                '"ReadytoEmail : " & ReadytoEmail & Environment.NewLine &
                '"SaveToDraft : " & SaveToDraft & Environment.NewLine &
                '"OpenEmailForm : " & OpenEmailForm & Environment.NewLine, vbOK, "Sending Now")
                Form1.txtResult.Text = "Preparing to send email via Office 365 - Email #" & xReportIndex.ToString
                'emailMessage.Send()

                Dim SentFolderForUser As FolderId = New FolderId(WellKnownFolderName.SentItems, userEmailAddress)
                emailMessage.SendAndSaveCopy(SentFolderForUser)
            Else
                Form1.txtResult.Text = "Preparing to save email via Office 365 - Email #" & xReportIndex.ToString
                emailMessage.Save()         'Save in Draft Folder
                'No Option to Open using EWS Method.


                'If OpenEmailForm Then
                'MsgBox("Draft and Display" & Environment.NewLine &
                '"ReadytoEmail : " & ReadytoEmail & Environment.NewLine &
                '"SaveToDraft : " & SaveToDraft & Environment.NewLine &
                '"OpenEmailForm : " & OpenEmailForm & Environment.NewLine, vbOK, "Draft and Display")
                'emailMessage
                'Else
                'MsgBox("Draft Only" & Environment.NewLine &
                '"ReadytoEmail : " & ReadytoEmail & Environment.NewLine &
                '"SaveToDraft : " & SaveToDraft & Environment.NewLine &
                '"OpenEmailForm : " & OpenEmailForm & Environment.NewLine, vbOK, "Draft Only")
                'OutlookMessage.Move(objFolder)
                'End If
            End If



        Catch exception As SmtpException
            Dim msg As String = "Mail cannot be sent (SmtpException): - Office365 - Email #" & xReportIndex.ToString
            msg += exception.Message
            Form1.txtResult.Text = "Office 365 Error : " & exception.Message & " - Email #" & xReportIndex.ToString
            Throw New Exception(msg)

        Catch exception As AutodiscoverRemoteException

            Dim msg As String = "Mail cannot be sent(AutodiscoverRemoteException): - Office365 - Email #" & xReportIndex.ToString
            msg += exception.Message
            Form1.txtResult.Text = "Office 365 Error : " & exception.Message & " - Email #" & xReportIndex.ToString
            Throw New Exception(msg)

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show("Error when trying to create Outlook Message" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Outlook")
            AddtoErrorLog("SendEmail", "Send_By_Office365", ex.Message, "Error when trying to create/send Office 365 Message", SendToString, userEmailAddress, Report(xReportIndex).EmailSubject, ReTryCount)

            If Not ErrorMessageExists Then
                Form1.txtResult.Text = "Office 365 Error : " & ex.Message
            End If

            If Not (ex.Message.Contains("Unauthorized")) Then
                If ReTryCount < 3 Then
                    ReTryCount += 1
                    'ReTry the send by Office365 if there was an error up to 5 times.
                    Threading.Thread.Sleep(500)
                    Send_By_EWS(xReportIndex, Office365Link)
                End If
            End If


        Finally
            If Not ErrorMessageExists Then
                Form1.txtResult.Text = "Email Sent/Saved via Office 365 - Email #" & xReportIndex.ToString
            End If
        End Try

    End Sub


    Private Async Function Send_By_MSGraph(ByVal xReportIndex As Integer) As Threading.Tasks.Task(Of Task)

        Dim InterAcctGraph As New InterAcctGraphDLL.Program()

        'Call InterAcctGraph.SendMail()

        'MsgBox("Sorting Records for MSGraph Sending", MsgBoxStyle.MsgBoxHelp, "IASendEmail - Send_By_MSGraph Start")

        Dim args() As String = {""}
        'Dim x As Integer = 0

        'Dim toRecipients(20) As Microsoft.Graph.Recipient
        Dim ToRecipientList = New List(Of Microsoft.Graph.Recipient)
        Dim CcRecipientList = New List(Of Microsoft.Graph.Recipient)
        Dim BccRecipientList = New List(Of Microsoft.Graph.Recipient)
        Dim FromEmail As New Microsoft.Graph.Recipient

        If Report(xReportIndex).EmailFrom.Length > 0 Then
            FromEmail = New Microsoft.Graph.Recipient With {.EmailAddress = New Microsoft.Graph.EmailAddress With {.Address = Report(xReportIndex).EmailFrom}}
        End If



        For x = 0 To Report(xReportIndex).EmailIndexCount
            If Report(xReportIndex).Email(x).EmailReceipt.Length > 0 Then   'Find if there is an Email Address

                Select Case Report(xReportIndex).Email(x).EmailSend 'To Send as 1.TO or 2.CC or 3.BCC or 4 for From
                    Case 2

                        CcRecipientList.Add(New Microsoft.Graph.Recipient With {.EmailAddress = New Microsoft.Graph.EmailAddress With {.Address = Report(xReportIndex).Email(x).EmailReceipt, .Name = Report(xReportIndex).Email(x).EmailFullName}})

                    Case 3
                        BccRecipientList.Add(New Microsoft.Graph.Recipient With {.EmailAddress = New Microsoft.Graph.EmailAddress With {.Address = Report(xReportIndex).Email(x).EmailReceipt, .Name = Report(xReportIndex).Email(x).EmailFullName}})
                    Case 4
                        FromEmail = New Microsoft.Graph.Recipient With {.EmailAddress = New Microsoft.Graph.EmailAddress With {.Address = Report(xReportIndex).Email(x).EmailReceipt, .Name = Report(xReportIndex).Email(x).EmailFullName}}

                    Case Else
                        ToRecipientList.Add(New Microsoft.Graph.Recipient With {.EmailAddress = New Microsoft.Graph.EmailAddress With {.Address = Report(xReportIndex).Email(x).EmailReceipt, .Name = Report(xReportIndex).Email(x).EmailFullName}})

                End Select
            End If
        Next


        'Looking at Attachments

        Dim attachments As Microsoft.Graph.MessageAttachmentsCollectionPage = New Microsoft.Graph.MessageAttachmentsCollectionPage()

        Dim xAttachedFileName As String = vbNullString
        Try
            If Report(xReportIndex).AttachIndexCount >= 0 Then   'PWR 25.11.2016 Make sure there are attachments.

                Dim ExtraHTMLFiles As Boolean = False


                For x As Integer = 0 To Report(xReportIndex).AttachIndexCount

                    If Not IsNothing(Report(xReportIndex).Attach(x).File) Then
                        If Report(xReportIndex).Attach(x).File.Length > 0 Then
                            If (InStr(LCase(Report(xReportIndex).Attach(x).File), ".htm") = 0) Or (ExtraHTMLFiles = True) Then   'Don't include the Base HTML File.

                                xAttachedFileName = vbNullString

                                If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                    xAttachedFileName = Report(xReportIndex).Attach(x).File
                                Else
                                    xAttachedFileName = (Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File)
                                End If

                                If File.Exists(xAttachedFileName) Then          'PWR 07.12.2016 Check if the File exists first before adding.
                                    Try
                                        Dim contentBytes As Byte() = System.IO.File.ReadAllBytes(xAttachedFileName)

                                        attachments.Add(New Microsoft.Graph.FileAttachment With {.Name = Report(xReportIndex).Attach(x).Name, .ContentBytes = contentBytes})


                                    Catch ex As Exception
                                        'System.Windows.Forms.MessageBox.Show("Error when trying to add attachements " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Office365")
                                        AddtoErrorLog("SendEmail", "Send_By_Office365 myAttachments.Add", ex.Message, "Error when trying to add attachements " & xAttachedFileName, "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
                                    Finally
                                        ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
                                    End Try
                                End If

                            Else
                                ExtraHTMLFiles = True
                            End If
                        Else
                            Exit For
                        End If
                    Else
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show("Error when trying to add attachements " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Office365")
            AddtoErrorLog("SendEmail", "Send_By_Office365", ex.Message, "Error when trying to add attachements " & xAttachedFileName, "", "", Report(xReportIndex).EmailSubject)
        Finally
            ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
        End Try



        'MsgBox("Just Before Linking to DLL", MsgBoxStyle.MsgBoxHelp, "IASendEmail - Send_By_MSGraph")

        'Form1.Show()


        Await InterAcctGraph.MainSendEmail(MsGraphGlobals.graphClient, MsGraphGlobals.MSALApplication, Report(xReportIndex).EmailSubject,
                                           Report(xReportIndex).EmailBody, ToRecipientList, CcRecipientList, BccRecipientList, FromEmail,
                                           MsGraphGlobals.tokenToUse, attachments, EmailBodyHTML,
                                           SaveToDraft, OpenEmailForm)


    End Function


    Private Sub Send_By_Outlook(ByVal xReportIndex As Integer)

        Dim ReadytoEmail As Boolean = False
        Dim SendToString As String = ""
        Dim DefaultEmailAddressTo As String = ""

        Dim OutlookMessage As Outlook.MailItem
        Dim AppOutlook As New Outlook.Application
        Dim myAttachments As Outlook.Attachments

        Dim objNS As Outlook._NameSpace = AppOutlook.Session
        Dim objFolder As Outlook.MAPIFolder
        objFolder = objNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDrafts)


        Try
            OutlookMessage = AppOutlook.CreateItem(Outlook.OlItemType.olMailItem)
            'Dim retValue As Boolean = False
            Dim Recipients As Outlook.Recipients = OutlookMessage.Recipients
            'Dim Recipients As Outlook.Recipients = Nothing
            'Dim recipientTo As Outlook.Recipient = Nothing
            'Dim recipientCC As Outlook.Recipient = Nothing
            'Dim recipientBCC As Outlook.Recipient = Nothing

            Try
                While Recipients.Count > 0
                    Recipients.Remove(1)
                End While

                For x As Integer = 0 To Report(xReportIndex).EmailIndexCount
                    If Not IsNothing(Report(xReportIndex).Email(x).EmailReceipt) Then   'Find if there is an Email Address

                        If (DefaultEmailAddressTo.Length = 0) Then
                            DefaultEmailAddressTo = Report(xReportIndex).Email(x).EmailReceipt
                        End If



                        Select Case Report(xReportIndex).Email(x).EmailSend 'To Send as 1.TO or 2.CC or 3.BCC
                            Case 2
                                'recipientCC.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                'recipientCC.Type = Outlook.OlMailRecipientType.olCC
                                'Recipients.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                'Recipients.Type = Outlook.OlMailRecipientType.olCC
                                AddRecipientToMailItem(OutlookMessage, Report(xReportIndex).Email(x).EmailReceipt, Outlook.OlMailRecipientType.olCC)
                            Case 3
                                'recipientBCC.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                'recipientBCC.Type = Outlook.OlMailRecipientType.olBCC
                                'Recipients.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                'Recipients.Type = Outlook.OlMailRecipientType.olBCC
                                AddRecipientToMailItem(OutlookMessage, Report(xReportIndex).Email(x).EmailReceipt, Outlook.OlMailRecipientType.olBCC)
                            Case 4

                            Case Else
                                'recipientTo.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                'recipientTo.Type = Outlook.OlMailRecipientType.olTo
                                'Recipients.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                'Recipients.Type = Outlook.OlMailRecipientType.olTo
                                AddRecipientToMailItem(OutlookMessage, Report(xReportIndex).Email(x).EmailReceipt, Outlook.OlMailRecipientType.olTo)
                        End Select
                        'retValue = Recipients.ResolveAll()

                        SendToString = SendToString & Report(xReportIndex).Email(x).EmailReceipt & " "
                        ReadytoEmail = True
                    Else
                        If x = 0 Then       'PWR 10.12.2018 If X is zero only.
                            OpenEmailForm = True
                        End If
                        Exit For
                    End If
                Next
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when trying to create Outlook Message" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Outlook")
                AddtoErrorLog("SendEmail", "Send_By_Outlook", ex.Message, "Error when trying to create Outlook Message", SendToString, Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Finally
                'If Not IsNothing(recipientBCC) Then Marshal.ReleaseComObject(recipientBCC)
                'If Not IsNothing(recipientCC) Then Marshal.ReleaseComObject(recipientCC)
                'If Not IsNothing(recipientTo) Then Marshal.ReleaseComObject(recipientTo)
                If Not IsNothing(Recipients) Then
                    Marshal.ReleaseComObject(Recipients)
                Else                'PWR 10.12.2018
                    OpenEmailForm = True                'PWR 06.12.2016
                End If
            End Try



            'Looking at Attachments
            Dim xAttachedFileName As String = vbNullString
            Try
                If Report(xReportIndex).AttachIndexCount >= 0 Then   'PWR 25.11.2016 Make sure there are attachments.
                    myAttachments = OutlookMessage.Attachments

                    Dim ExtraHTMLFiles As Boolean = False


                    For x As Integer = 0 To Report(xReportIndex).AttachIndexCount

                        If Not IsNothing(Report(xReportIndex).Attach(x).File) Then
                            If Report(xReportIndex).Attach(x).File.Length > 0 Then
                                If (InStr(LCase(Report(xReportIndex).Attach(x).File), ".htm") = 0) Or (ExtraHTMLFiles = True) Then   'Don't include the Base HTML File.

                                    xAttachedFileName = vbNullString

                                    If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                        xAttachedFileName = Report(xReportIndex).Attach(x).File
                                    Else
                                        xAttachedFileName = (Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File)
                                    End If

                                    If File.Exists(xAttachedFileName) Then          'PWR 07.12.2016 Check if the File exists first before adding.
                                        Try
                                            myAttachments.Add(xAttachedFileName)
                                        Catch ex As Exception
                                            'System.Windows.Forms.MessageBox.Show("Error when trying to add attachements " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Outlook")
                                            AddtoErrorLog("SendEmail", "Send_By_Outlook myAttachments.Add", ex.Message, "Error when trying to add attachements " & xAttachedFileName, "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
                                        Finally
                                            ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
                                        End Try
                                    End If

                                Else
                                    ExtraHTMLFiles = True
                                End If
                            Else
                                Exit For
                            End If
                        Else
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when trying to add attachements " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Outlook")
                AddtoErrorLog("SendEmail", "Send_By_Outlook", ex.Message, "Error when trying to add attachements " & xAttachedFileName, DefaultEmailAddressTo, Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Finally
                ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
            End Try

            If (Report(xReportIndex).EmailFrom <> "") Then

                'Dim oAccount As Outlook.Account
                ' Dim olMailItem As Outlook.MailItem
                'Dim olMailItem As Outlook.MailItem

                '                olEas()
                '4:
                '                An account that uses Exchange ActiveSync (EAS) on mobile devices.
                '                olExchange()
                '0:
                'An Exchange account.
                '                olHttp()
                '3:
                'An HTTP account.
                '                olImap()
                '1:
                'An IMAP account.
                '                olOtherAccount()
                '5:
                'Other or unknown account.
                '                olPop3()
                '2:
                'A POP3 account.


                'For Each oAccount In objNS.Session.Accounts

                'If oAccount.SmtpAddress = Report(xReportIndex).EmailFrom Then

                '                       OutlookMessage.SendUsingAccount = oAccount
                '
                '                    End If

                'Next

                OutlookMessage.SentOnBehalfOfName = Report(xReportIndex).EmailFrom
            End If

            If (Report(xReportIndex).EmailReplyTo <> "") Then           'PWR 31.10.2017 Email Reply To Address
                OutlookMessage.ReplyRecipients.Add(Report(xReportIndex).EmailReplyTo)
            End If

            OutlookMessage.Subject = Report(xReportIndex).EmailSubject
            OutlookMessage.Body = Replace(EmailBodyPlain, "|", vbNewLine)      'Plain Text Body
            If EmailBodyHTML = "" Then
                OutlookMessage.BodyFormat = Outlook.OlBodyFormat.olFormatPlain
            Else
                OutlookMessage.BodyFormat = Outlook.OlBodyFormat.olFormatHTML
                OutlookMessage.HTMLBody = EmailBodyHTML
            End If

            Form1.txtResult.Text = "Preparing to send email via Outlook"

            If ReadytoEmail And SaveToDraft = False Then
                'MsgBox("Sending Now" & Environment.NewLine &
                '"ReadytoEmail : " & ReadytoEmail & Environment.NewLine &
                '"SaveToDraft : " & SaveToDraft & Environment.NewLine &
                '"OpenEmailForm : " & OpenEmailForm & Environment.NewLine, vbOK, "Sending Now")

                Form1.txtResult.Text = "Preparing to send email via Outlook - Email #" & xReportIndex.ToString
                OutlookMessage.Send()
            Else
                OutlookMessage.Save()          'Save in Draft Folder
                If OpenEmailForm Then
                    'MsgBox("Draft and Display" & Environment.NewLine &
                    '"ReadytoEmail : " & ReadytoEmail & Environment.NewLine &
                    '"SaveToDraft : " & SaveToDraft & Environment.NewLine &
                    '"OpenEmailForm : " & OpenEmailForm & Environment.NewLine, vbOK, "Draft and Display")

                    Form1.txtResult.Text = "Preparing to save and open email via Outlook - Email #" & xReportIndex.ToString
                    OutlookMessage.Display(False)
                Else
                    'MsgBox("Draft Only" & Environment.NewLine &
                    '"ReadytoEmail : " & ReadytoEmail & Environment.NewLine &
                    '"SaveToDraft : " & SaveToDraft & Environment.NewLine &
                    '"OpenEmailForm : " & OpenEmailForm & Environment.NewLine, vbOK, "Draft Only")
                    Form1.txtResult.Text = "Preparing to save email via Outlook"
                    'PWR 01.02.2022 Turn off the Move Function as it is not needed, the Save will already save above into the Drafts Folder.
                    'OutlookMessage.Move(objFolder)
                End If
            End If

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show("Mail could not be sent" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_Outlook") 'if you dont want this message, simply delete this line    
            AddtoErrorLog("SendEmail", "Send_By_Outlook", ex.Message, String.Format("Mail could not be sent - ReadytoEmail {0} SaveToDraft {1}", ReadytoEmail, SaveToDraft), DefaultEmailAddressTo, Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Form1.txtResult.Text = "Error : " & ex.Message & " - Email #" & xReportIndex.ToString
            OutlookMessage = Nothing
            AppOutlook = Nothing
            objFolder = Nothing
            objNS = Nothing
        Finally
            Form1.txtResult.Text = "Email Sent/Saved via Outlook - Email #" & xReportIndex.ToString
            OutlookMessage = Nothing
            AppOutlook = Nothing
            objFolder = Nothing
            objNS = Nothing
        End Try
    End Sub

    Private Sub Send_By_SMTP(ByVal xReportIndex As Integer)
        'create the mail message
        Dim mail As New MailMessage()

        Dim ReadytoEmail As Boolean = False

        'set the addresses
        If Show_SendButton() = False Then
            Exit Sub
        End If

        mail.From = New MailAddress(My.Settings.SMTPEmail)

        Try

            Try

                For x As Integer = 0 To Report(xReportIndex).EmailIndexCount
                    If Report(xReportIndex).Email(x).EmailReceipt.Length > 0 Then   'Find if there is an Email Address

                        Select Case Report(xReportIndex).Email(x).EmailSend 'To Send as 1.TO or 2.CC or 3.BCC
                            Case 2
                                mail.CC.Add(Report(xReportIndex).Email(x).EmailReceipt)

                            Case 3
                                mail.Bcc.Add(Report(xReportIndex).Email(x).EmailReceipt)
                            Case 4
                                mail.From.Equals(Report(xReportIndex).Email(x).EmailReceipt)    'PWR 15.09.2017 try to set the from address

                            Case Else
                                mail.To.Add(Report(xReportIndex).Email(x).EmailReceipt)
                        End Select
                        'retValue = Recipients.ResolveAll()
                        ReadytoEmail = True
                    Else
                        Exit For
                    End If
                Next
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when attaching Receipts" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_SMTP")
                AddtoErrorLog("SendEmail", "Send_By_SMTP", ex.Message, "Error when attaching Receipts", "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            End Try

            'Looking at Attachments
            Try

                Dim ExtraHTMLFiles As Boolean = False

                For x As Integer = 0 To Report(xReportIndex).AttachIndexCount

                    If Report(xReportIndex).Attach(x).File IsNot Nothing Then           'PWR 14.02.2019 Check if the FIle is not nothing
                        If Report(xReportIndex).Attach(x).File.Length > 0 Then
                            If (InStr(LCase(Report(xReportIndex).Attach(x).File), ".htm") = 0) Or (ExtraHTMLFiles = True) Then   'Don't include the Base HTML File.
                                Dim attachment As System.Net.Mail.Attachment
                                If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                    attachment = New System.Net.Mail.Attachment(Report(xReportIndex).Attach(x).File)
                                Else
                                    attachment = New System.Net.Mail.Attachment(Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File)
                                End If

                                mail.Attachments.Add(attachment)
                            Else
                                ExtraHTMLFiles = True
                            End If
                        Else
                            Exit For
                        End If
                    Else
                        Exit For
                    End If

                Next
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when attaching Attachments" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_SMTP")
                AddtoErrorLog("SendEmail", "Send_By_SMTP", ex.Message, "Error when attaching Attachments", "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Finally
                ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
            End Try

            If Len(Report(xReportIndex).EmailFrom) > 0 Then
                mail.From = New MailAddress(Report(xReportIndex).EmailFrom)
            Else
                'PWR 29.10.2024 Added in to Force a From Address
                mail.From = New MailAddress(My.Settings.SMTPEmail)
            End If

            If Len(Report(xReportIndex).EmailReplyTo) > 0 Then      'PWR 31.10.2017 New Code for Reply To Address.
                mail.ReplyToList.Add(New MailAddress(Report(xReportIndex).EmailReplyTo))
            Else
                'PWR 29.10.2024 Added in to Force a From Address
                mail.ReplyToList.Add(New MailAddress(My.Settings.SMTPEmail))
            End If

            'set the content
            mail.Subject = Report(xReportIndex).EmailSubject
            'mail.Body = EmailBodyPlain        'Plain Text Body
            mail.Body = EmailBodyHTML
            mail.IsBodyHtml = True

            'send the message
            If ReadytoEmail Then

                If My.Settings.SMTPSSL Then
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    'ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault
                Else
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls12 Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls)
                End If

                'PWR 29.10.2024 Check and use the SSL Port Number
                Dim SMPPort As Integer = Int(My.Settings.SMTPPort)
                If SMPPort > 0 Then
                    Dim smtp As New SmtpClient(My.Settings.SMTPServer, SMPPort)
                    'smtp.Credentials = CredentialCache.DefaultNetworkCredentials
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network
                    smtp.UseDefaultCredentials = False
                    smtp.Credentials = New System.Net.NetworkCredential(My.Settings.SMTPEmail, Decrypt(My.Settings.Password))
                    smtp.EnableSsl = My.Settings.SMTPSSL
                    'smtp.TargetName = String.Format("STARTTLS/{0}", My.Settings.SMTPServer)
                    'smtp.Port = SMPPort
                    'smtp.Host = My.Settings.SMTPServer
                    Form1.txtResult.Text = "Preparing to send email via SMTP - Email #" & xReportIndex.ToString
                    smtp.Send(mail)
                Else
                    Dim smtp As New SmtpClient(My.Settings.SMTPServer)

                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network
                    smtp.UseDefaultCredentials = False
                    smtp.Credentials = New System.Net.NetworkCredential(My.Settings.SMTPEmail, Decrypt(My.Settings.Password))
                    smtp.EnableSsl = My.Settings.SMTPSSL

                    Form1.txtResult.Text = "Preparing to send email via SMTP - Email #" & xReportIndex.ToString
                    smtp.Send(mail)
                End If
                SaveSettings()

                'My.Settings.SMTPServerLast = My.Settings.SMTPServer
                'My.Settings.Save()

            End If

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show("Mail could not be sent" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_SMTP") 'if you dont want this message, simply delete this line    
            AddtoErrorLog("SendEmail", "Send_By_SMTP", ex.Message, "Mail could not be sent", "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Form1.txtResult.Text = "SMTP Error : " & ex.Message & " - Email #" & xReportIndex.ToString
        Finally
            Form1.txtResult.Text = "Email Sent via SMTP - Email #" & xReportIndex.ToString
            mail = Nothing
        End Try
    End Sub


    Private Sub Send_By_TPDuet(ByVal xReportIndex As Integer)
        'create the mail message for TPDuel for NetLeverage

        Dim RunString As String = "mailto:"

        Dim ReadytoEmail As Boolean = False

        Dim SendToCount As Integer = 0
        Dim AttachToCount As Integer = 0


        Try

            Try

                For x As Integer = 0 To Report(xReportIndex).EmailIndexCount
                    If Report(xReportIndex).Email(x).EmailReceipt.Length > 0 Then   'Find if there is an Email Address

                        Select Case Report(xReportIndex).Email(x).EmailSend 'To Send as 1.TO or 2.CC or 3.BCC
                            Case 2
                                'mail.CC.Add(Report(xReportIndex).Email(x).EmailReceipt)

                            Case 3
                                'mail.Bcc.Add(Report(xReportIndex).Email(x).EmailReceipt)
                            Case 4
                                'mail.From.Equals(Report(xReportIndex).Email(x).EmailReceipt)    'PWR 15.09.2017 try to set the from address

                            Case Else
                                'mail.To.Add(Report(xReportIndex).Email(x).EmailReceipt)
                                If SendToCount = 0 Then
                                    RunString += Report(xReportIndex).Email(x).EmailReceipt
                                End If
                                SendToCount += 1

                        End Select
                        'retValue = Recipients.ResolveAll()
                        ReadytoEmail = True
                    Else
                        Exit For
                    End If
                Next
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when attaching Receipts" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_SMTP")
                AddtoErrorLog("SendEmail", "Send_By_TPDuet", ex.Message, "Error when attaching Receipts", "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            End Try

            'Looking at Attachments
            Try

                Dim ExtraHTMLFiles As Boolean = False

                For x As Integer = 0 To Report(xReportIndex).AttachIndexCount

                    If Report(xReportIndex).Attach(x).File IsNot Nothing Then           'PWR 14.02.2019 Check if the FIle is not nothing
                        If Report(xReportIndex).Attach(x).File.Length > 0 Then
                            If (InStr(LCase(Report(xReportIndex).Attach(x).File), ".htm") = 0) Or (ExtraHTMLFiles = True) Then   'Don't include the Base HTML File.

                                If AttachToCount = 0 Then
                                    RunString += "?Attachment="
                                    If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                        RunString += Report(xReportIndex).Attach(x).File
                                    Else
                                        RunString += WebUtility.HtmlEncode(Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File)
                                    End If
                                End If
                                AttachToCount += 1

                            Else
                                ExtraHTMLFiles = True
                            End If
                        Else
                            Exit For
                        End If
                    Else
                        Exit For
                    End If

                Next
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show("Error when attaching Attachments" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_SMTP")
                AddtoErrorLog("SendEmail", "Send_By_TPDuet", ex.Message, "Error when attaching Attachments", "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Finally
                ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
            End Try

            If Len(Report(xReportIndex).EmailFrom) > 0 Then
                'mail.From = New MailAddress(Report(xReportIndex).EmailFrom)
            End If

            If Len(Report(xReportIndex).EmailReplyTo) > 0 Then      'PWR 31.10.2017 New Code for Reply To Address.
                'mail.ReplyToList.Add(New MailAddress(Report(xReportIndex).EmailReplyTo))
            End If

            'set the content
            RunString += "?subject=" & WebUtility.HtmlEncode(Report(xReportIndex).EmailSubject)
            RunString += "&body=" & WebUtility.HtmlEncode(EmailBodyHTML)
            'mail.IsBodyHtml = True

            'send the message
            If ReadytoEmail Then

                Form1.txtResult.Text = "Preparing to send email via TPDuet"
                'TPDuet.exe "mailto:sales@netleverage.com?subject=Sales%20Invoice&body=Here%20is%20the%20invoice.%0ARegards%20?Attachment=File_path"
                RunString = "TPDuet.exe " & Chr(34) & RunString & Chr(34)
                Shell(RunString, AppWinStyle.NormalFocus)

            End If

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show("Mail could not be sent" & vbNewLine & ex.Message, ApplicationTitle & " : Send_By_TPDuet") 'if you dont want this message, simply delete this line    
            AddtoErrorLog("SendEmail", "Send_By_TPDuet", ex.Message, "Mail could not be sent", "", Report(xReportIndex).EmailFrom, Report(xReportIndex).EmailSubject)
            Form1.txtResult.Text = "TPDuet Error : " & ex.Message
        Finally
            Form1.txtResult.Text = "Email Sent via Outlook (via TPDuet)"
        End Try



    End Sub


    Private Async Function Bulk_EmailReport(ByVal xReportIndex As Integer) As Threading.Tasks.Task(Of Task)

        EmailBodyHTML = ""

        If Report(xReportIndex).AttachIndexCount >= 0 Then
            Dim builder As New StringBuilder

            Form1.txtResult.Text = "Generating Email #" & xReportIndex.ToString

            For x As Integer = 0 To Report(xReportIndex).AttachIndexCount

                If Report(xReportIndex).Attach(x).File IsNot Nothing Then       'PWR 14.02.2019 Stop the error message if the Report has nothing in it.
                    If Report(xReportIndex).Attach(x).File.Length > 0 Then
                        If InStr(LCase(Report(xReportIndex).Attach(x).File), LCase(".htm")) Then
                            Dim HtmlFile As String = Report(xReportIndex).Attach(x).File
                            If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                HtmlFile = Report(xReportIndex).Attach(x).File
                            Else
                                HtmlFile = Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File
                            End If

                            Using reader As StreamReader = New StreamReader(HtmlFile)
                                Dim xline As String
                                ' Read one line from file
                                xline = reader.ReadLine
                                ' Loop over each line in file, While list is Not Nothing.
                                Do While (Not xline Is Nothing)
                                    ' Add this line to list.
                                    builder.Append(xline)
                                    'Get the next line
                                    xline = reader.ReadLine
                                Loop
                            End Using

                            Exit For
                        End If
                    Else
                        Exit For
                    End If
                Else
                    Exit For
                End If

            Next

            EmailBodyHTML = builder.ToString

        End If

        EmailBodyPlain = Report(xReportIndex).EmailBody         'PWR 25.11.2016 Get the Plain Text Version

        'PWR 13.10.2021 Restart the ReTry Counter.. And run till 5.
        ReTryCount = 0

        'PWR 01.02.2022 Find out if this Command has been defined in the Command Arguements.
        If (OpenEmailFormCommand = False) Then
            OpenEmailForm = False   'PWR 28.01.2021 Force the Run back to False for each Sending line.
        End If

        'MsgBox("Collected Data - Report Number " + xReportIndex.ToString(), MsgBoxStyle.MsgBoxHelp, "IASendEmail - Bulk_EmailReport")


        Select Case Find_Current_EmailMethod()
            Case Methods.Outlook : Call Send_By_Outlook(xReportIndex)
            Case Methods.Office365 : Call Send_By_EWS(xReportIndex, True)
            Case Methods.EWS : Call Send_By_EWS(xReportIndex, False)
            Case Methods.TPDuet : Call Send_By_TPDuet(xReportIndex)
            Case Methods.MSGraph : Return Await Send_By_MSGraph(xReportIndex)
            Case Else
                Call Send_By_SMTP(xReportIndex)
        End Select



    End Function

    Public Function Find_Current_EmailMethod() As Methods

        If Form1.EmailMethod_Outlook.Checked Then
            Return Methods.Outlook
        End If
        If Form1.EmailMethod_SMTP.Checked Then
            Return Methods.SMTP
        End If
        If Form1.EmailMethod_Office365.Checked Then
            Return Methods.Office365
        End If
        If Form1.EmailMethod_EWS.Checked Then
            Return Methods.EWS
        End If
        If Form1.EmailMethod_TPDuet.Checked Then
            Return Methods.TPDuet
        End If
        If Form1.EmailMethod_MSGraph.Checked Then
            Return Methods.MSGraph
        End If

        Return Methods.SMTP

    End Function


    Public Sub Show_SMTPFields()
        Dim EmailMethod As Integer = Find_Current_EmailMethod()

        Dim showGroupBox As Boolean = ((EmailMethod = Methods.SMTP) Or (EmailMethod = Methods.Office365) Or (EmailMethod = Methods.MSGraph) Or (EmailMethod = Methods.EWS))

        Form1.GroupBox1.Visible = showGroupBox

        'Form1.btnDraft.Enabled = (My.Settings.EmailMethod <> 1)          'Only show Draft Option if it is Outlook
        Form1.txtSMTP_Email.Visible = showGroupBox
        Form1.lblSMTP_Email.Visible = showGroupBox
        Form1.txtSMTP_Server.Visible = showGroupBox
        Form1.lblSMTP_Server.Visible = showGroupBox

        Form1.txtSMTP_Server.Enabled = ((EmailMethod <> Methods.Office365) Or (EmailMethod <> Methods.MSGraph))

        Dim showPassword As Boolean = ((EmailMethod = Methods.Office365) Or (EmailMethod = Methods.EWS) Or (EmailMethod = Methods.SMTP))

        Form1.txtPassword.Visible = showPassword
        Form1.lblPassword.Visible = showPassword
        Form1.btnShow.Visible = showPassword

        Dim showSMTP As Boolean = (EmailMethod = Methods.SMTP)

        Form1.TextBoxPort.Visible = showSMTP
        Form1.CheckBoxSSL.Visible = showSMTP
        Form1.LabelSMTPPort.Visible = showSMTP

        If EmailMethod = Methods.Office365 Then
            Form1.txtSMTP_Server.Text = "https://outlook.office365.com/ews/exchange.asmx"
        Else
            If EmailMethod = Methods.MSGraph Then
                Form1.txtSMTP_Server.Text = "https://graph.microsoft.com/v1.0/"
            Else
                If My.Settings.SMTPServer.Length > 0 Then
                    Form1.txtSMTP_Server.Text = My.Settings.SMTPServer
                Else
                    If ((Form1.txtSMTP_Server.Text = "https://outlook.office365.com/ews/exchange.asmx") Or (Form1.txtSMTP_Server.Text = "https://graph.microsoft.com/v1.0/")) Then
                        Form1.txtSMTP_Server.Text = ""
                    End If
                End If
            End If
        End If


    End Sub

    Public Function Show_SendButton() As Boolean
        Dim EmailMethod As Integer = Find_Current_EmailMethod()

        If ((InStr(Form1.txtSMTP_Server.Text, ".") > 0) Or
            (EmailMethod <> Methods.SMTP)) And ((Form1.txtPassword.Text.Length > 0) Or
            (EmailMethod <> Methods.Office365)) And (InStr(Form1.txtSMTP_Email.Text, "@") > 0 And InStr(Form1.txtSMTP_Email.Text, ".") > 0) Or
            EmailMethod = Methods.Outlook Or EmailMethod = Methods.EWS Or EmailMethod = Methods.MSGraph Then
            Show_SendButton = True
            Form1.btnDraft.Visible = (EmailMethod <> Methods.SMTP)
        Else
            Show_SendButton = False
            Form1.btnDraft.Visible = False
        End If
        Form1.btnSend.Visible = Show_SendButton
        Form1.btnDraftOpen.Visible = (Form1.EmailMethod_Outlook.Checked = False)

        If Report Is Nothing Then    'Only need to load once, or if the data has been removed.
            Form1.btnSend.Text = "Open Report && Send"
            Form1.btnDraft.Text = "Open Report && Save"
            Form1.btnDraftOpen.Text = "Open Report && Save"
        Else
            Form1.btnSend.Text = "Send Email"
            Form1.btnDraft.Text = "Save to Draft"
            Form1.btnDraftOpen.Text = "Save to Draft && Open"
        End If




    End Function


    ''' <summary>
    ''' Routine to Delete the Security Sensitive Files after they have been emailed so they don't sit on the Server.
    ''' </summary>
    Private Sub DeleteSecurePDFs()

        Dim xAttachedFileName As String = ""
        Dim xAttachCounter As Integer = 0
        Try
            For xReportIndex As Integer = 0 To UBound(Report)
                If Not Report(xReportIndex).MainEmailAddress Is Nothing Then         'Only do those which have an email address
                    xAttachedFileName = ""
                    If Report(xReportIndex).AttachIndexCount >= 0 Then   'PWR 25.11.2016 Make sure there are attachments.
                        For x As Integer = 0 To Report(xReportIndex).AttachIndexCount
                            If Report(xReportIndex).Attach(x).SecureReport = True Then
                                'This PDF needs to be deleted
                                xAttachedFileName = ""
                                xAttachCounter += 1
                                If Report(xReportIndex).Attach(x).File IsNot Nothing Then       'PWR 14.02.2019 Check if the File is not blank
                                    If Report(xReportIndex).Attach(x).File.Length > 0 Then
                                        If Mid$(Report(xReportIndex).Attach(x).File, 1, 2) = "\\" Or Mid$(Report(xReportIndex).Attach(x).File, 2, 1) = ":" Then
                                            xAttachedFileName = Report(xReportIndex).Attach(x).File
                                        Else
                                            xAttachedFileName = (Report(xReportIndex).Attach(x).Directory & Report(xReportIndex).Attach(x).File)
                                        End If

                                        If xAttachedFileName.Length > 0 Then
                                            My.Computer.FileSystem.DeleteFile(xAttachedFileName)
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            Next

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show("Error when trying to delete secure PDF Files " & vbNewLine & xAttachedFileName & vbNewLine & ex.Message & vbNewLine & "Attachment Count: " & xAttachCounter.ToString, ApplicationTitle & " : DeleteSecurePDFs")
            AddtoErrorLog("SendEmail", "DeleteSecurePDFs", ex.Message, "Error when trying to delete secure PDF Files " & xAttachedFileName & "Attachment Count: " & xAttachCounter.ToString)
        Finally
            ' If Not IsNothing(myAttachments) Then Marshal.ReleaseComObject(myAttachments)
        End Try

    End Sub

    Public Sub ShowButtons(showNow As Boolean)
        Form1.btnDraft.Visible = showNow
        Form1.btnDraftOpen.Visible = showNow
        Form1.btnSave.Visible = showNow
        Form1.btnSend.Visible = showNow
        Form1.btnShow.Visible = showNow
        Form1.GroupBox1.Visible = showNow
        Form1.GroupBox2.Visible = showNow
        Form1.GroupBox4.Visible = Not showNow
        If showNow Then
            Call Show_SendButton()
        End If
    End Sub


    Public Async Function BulkSendPrint() As Threading.Tasks.Task(Of Task)

        If Report Is Nothing Then
            'MsgBox("No Report Found", MessageBoxButtons.OK, "IASendEmail - BulkSendPrint")
            Exit Function
        End If

        Dim EmailMethod As Integer = Find_Current_EmailMethod()

        Form1.txtResult.Text = "Generating Reports to Email"

        If (EmailMethod = Methods.SMTP) And ((My.Settings.SMTPEmail.Length = 0 Or My.Settings.SMTPServer.Length = 0)) Then
            If Show_SendButton() = True Then
                SaveSettings()
            Else
                MsgBox("SMTP Settings are not correct." & vbNewLine & vbNewLine & "Please correct and ReTry.", vbCritical, "SMTP Settings are not Valid.")
                Exit Function
            End If
        End If

        'MsgBox("Starting Loop for Sending", MessageBoxButtons.OK, "IASendEmail - BulkSendPrint - Start Loop")

        ShowButtons(False)


        For x As Integer = 0 To UBound(Report)

            If (ErrorForceStop) Then
                'MsgBox("Error Occured", MsgBoxStyle.MsgBoxHelp, "IASendEmail - ErrorForceStop")
                Exit For
            End If

            If Not Report(x).MainEmailAddress Is Nothing Then         'Only do those which have an email address
                'MsgBox("Report Number " + x.ToString(), MsgBoxStyle.MsgBoxHelp, "IASendEmail - Running Report")
                Await Bulk_EmailReport(x)
            End If
        Next

        'MsgBox("Finished Loop for Sending", MessageBoxButtons.OK, "IASendEmail - BulkSendPrint - Finished Loop")

        'After Sending, check if the PDFs need deleting if they are Secure PDFs.
        DeleteSecurePDFs()

        ShowButtons(True)

        If (ErrorMessageExists) Then
            MsgBox(ErrorMessageText, MsgBoxStyle.MsgBoxHelp, "IASendEmail - Error Sending Email")
        End If


    End Function


End Module
