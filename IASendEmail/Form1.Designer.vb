﻿

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents EmailMethod_Outlook As System.Windows.Forms.RadioButton
    Friend WithEvents lblSMTP_Server As System.Windows.Forms.Label
    Friend WithEvents lblSMTP_Email As System.Windows.Forms.Label
    Friend WithEvents txtSMTP_Server As System.Windows.Forms.TextBox
    Friend WithEvents txtSMTP_Email As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnDraft As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnDraftOpen As System.Windows.Forms.Button
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtResult As System.Windows.Forms.TextBox

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btnSend = New System.Windows.Forms.Button()
        Me.EmailMethod_Outlook = New System.Windows.Forms.RadioButton()
        Me.lblSMTP_Server = New System.Windows.Forms.Label()
        Me.lblSMTP_Email = New System.Windows.Forms.Label()
        Me.txtSMTP_Server = New System.Windows.Forms.TextBox()
        Me.txtSMTP_Email = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnDraft = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnDraftOpen = New System.Windows.Forms.Button()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.EmailMethod_EWS = New System.Windows.Forms.RadioButton()
        Me.EmailMethod_TPDuet = New System.Windows.Forms.RadioButton()
        Me.EmailMethod_MSGraph = New System.Windows.Forms.RadioButton()
        Me.EmailMethod_Office365 = New System.Windows.Forms.RadioButton()
        Me.EmailMethod_SMTP = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtResult = New System.Windows.Forms.TextBox()
        Me.CheckBoxSSL = New System.Windows.Forms.CheckBox()
        Me.LabelSMTPPort = New System.Windows.Forms.Label()
        Me.TextBoxPort = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(688, 15)
        Me.btnSend.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(176, 60)
        Me.btnSend.TabIndex = 0
        Me.btnSend.Text = "Send Email"
        '
        'EmailMethod_Outlook
        '
        Me.EmailMethod_Outlook.Location = New System.Drawing.Point(18, 27)
        Me.EmailMethod_Outlook.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EmailMethod_Outlook.Name = "EmailMethod_Outlook"
        Me.EmailMethod_Outlook.Size = New System.Drawing.Size(352, 33)
        Me.EmailMethod_Outlook.TabIndex = 2
        Me.EmailMethod_Outlook.Text = "Outlook Client"
        '
        'lblSMTP_Server
        '
        Me.lblSMTP_Server.Location = New System.Drawing.Point(39, 27)
        Me.lblSMTP_Server.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lblSMTP_Server.Name = "lblSMTP_Server"
        Me.lblSMTP_Server.Size = New System.Drawing.Size(107, 24)
        Me.lblSMTP_Server.TabIndex = 3
        Me.lblSMTP_Server.Text = "Server"
        '
        'lblSMTP_Email
        '
        Me.lblSMTP_Email.Location = New System.Drawing.Point(39, 93)
        Me.lblSMTP_Email.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lblSMTP_Email.Name = "lblSMTP_Email"
        Me.lblSMTP_Email.Size = New System.Drawing.Size(107, 19)
        Me.lblSMTP_Email.TabIndex = 4
        Me.lblSMTP_Email.Text = "Email"
        '
        'txtSMTP_Server
        '
        Me.txtSMTP_Server.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSMTP_Server.Location = New System.Drawing.Point(18, 47)
        Me.txtSMTP_Server.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtSMTP_Server.Name = "txtSMTP_Server"
        Me.txtSMTP_Server.Size = New System.Drawing.Size(531, 36)
        Me.txtSMTP_Server.TabIndex = 5
        '
        'txtSMTP_Email
        '
        Me.txtSMTP_Email.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSMTP_Email.Location = New System.Drawing.Point(18, 122)
        Me.txtSMTP_Email.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtSMTP_Email.Name = "txtSMTP_Email"
        Me.txtSMTP_Email.Size = New System.Drawing.Size(636, 36)
        Me.txtSMTP_Email.TabIndex = 6
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'btnDraft
        '
        Me.btnDraft.Location = New System.Drawing.Point(688, 85)
        Me.btnDraft.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnDraft.Name = "btnDraft"
        Me.btnDraft.Size = New System.Drawing.Size(176, 57)
        Me.btnDraft.TabIndex = 7
        Me.btnDraft.Text = "Save to Draft"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(688, 474)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(176, 63)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Cancel"
        '
        'btnDraftOpen
        '
        Me.btnDraftOpen.Location = New System.Drawing.Point(688, 152)
        Me.btnDraftOpen.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnDraftOpen.Name = "btnDraftOpen"
        Me.btnDraftOpen.Size = New System.Drawing.Size(176, 68)
        Me.btnDraftOpen.TabIndex = 9
        Me.btnDraftOpen.Text = "Save to Draft && Open"
        '
        'lblPassword
        '
        Me.lblPassword.Location = New System.Drawing.Point(39, 168)
        Me.lblPassword.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(122, 19)
        Me.lblPassword.TabIndex = 10
        Me.lblPassword.Text = "Password"
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(18, 197)
        Me.txtPassword.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(636, 36)
        Me.txtPassword.TabIndex = 11
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBoxPort)
        Me.GroupBox1.Controls.Add(Me.CheckBoxSSL)
        Me.GroupBox1.Controls.Add(Me.LabelSMTPPort)
        Me.GroupBox1.Controls.Add(Me.btnShow)
        Me.GroupBox1.Controls.Add(Me.lblSMTP_Server)
        Me.GroupBox1.Controls.Add(Me.lblSMTP_Email)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.txtSMTP_Server)
        Me.GroupBox1.Controls.Add(Me.lblPassword)
        Me.GroupBox1.Controls.Add(Me.txtSMTP_Email)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 199)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(669, 250)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Login Settings"
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(478, 168)
        Me.btnShow.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(176, 30)
        Me.btnShow.TabIndex = 15
        Me.btnShow.Text = "Show Password"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.EmailMethod_EWS)
        Me.GroupBox2.Controls.Add(Me.EmailMethod_TPDuet)
        Me.GroupBox2.Controls.Add(Me.EmailMethod_MSGraph)
        Me.GroupBox2.Controls.Add(Me.EmailMethod_Office365)
        Me.GroupBox2.Controls.Add(Me.EmailMethod_SMTP)
        Me.GroupBox2.Controls.Add(Me.EmailMethod_Outlook)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 15)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(669, 142)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Email Method"
        '
        'EmailMethod_EWS
        '
        Me.EmailMethod_EWS.Location = New System.Drawing.Point(393, 93)
        Me.EmailMethod_EWS.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EmailMethod_EWS.Name = "EmailMethod_EWS"
        Me.EmailMethod_EWS.Size = New System.Drawing.Size(269, 33)
        Me.EmailMethod_EWS.TabIndex = 7
        Me.EmailMethod_EWS.Text = "EWS (Exchange Web Server)"
        '
        'EmailMethod_TPDuet
        '
        Me.EmailMethod_TPDuet.Location = New System.Drawing.Point(18, 93)
        Me.EmailMethod_TPDuet.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EmailMethod_TPDuet.Name = "EmailMethod_TPDuet"
        Me.EmailMethod_TPDuet.Size = New System.Drawing.Size(352, 33)
        Me.EmailMethod_TPDuet.TabIndex = 6
        Me.EmailMethod_TPDuet.Text = "Outlook (via TPDuet on NetLeverage Cloud)"
        '
        'EmailMethod_MSGraph
        '
        Me.EmailMethod_MSGraph.Location = New System.Drawing.Point(393, 60)
        Me.EmailMethod_MSGraph.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EmailMethod_MSGraph.Name = "EmailMethod_MSGraph"
        Me.EmailMethod_MSGraph.Size = New System.Drawing.Size(261, 33)
        Me.EmailMethod_MSGraph.TabIndex = 5
        Me.EmailMethod_MSGraph.Text = "Office 365 (MS Graph)"
        '
        'EmailMethod_Office365
        '
        Me.EmailMethod_Office365.Location = New System.Drawing.Point(18, 60)
        Me.EmailMethod_Office365.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EmailMethod_Office365.Name = "EmailMethod_Office365"
        Me.EmailMethod_Office365.Size = New System.Drawing.Size(352, 33)
        Me.EmailMethod_Office365.TabIndex = 4
        Me.EmailMethod_Office365.Text = "Office 365 (WebMail)"
        '
        'EmailMethod_SMTP
        '
        Me.EmailMethod_SMTP.Location = New System.Drawing.Point(393, 27)
        Me.EmailMethod_SMTP.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EmailMethod_SMTP.Name = "EmailMethod_SMTP"
        Me.EmailMethod_SMTP.Size = New System.Drawing.Size(261, 33)
        Me.EmailMethod_SMTP.TabIndex = 3
        Me.EmailMethod_SMTP.Text = "SMTP"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Location = New System.Drawing.Point(797, 264)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(862, 448)
        Me.GroupBox4.TabIndex = 16
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Sending Email"
        Me.GroupBox4.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(27, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(642, 32)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Authorisation Check/Login before sending emails."
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(688, 382)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(176, 63)
        Me.btnSave.TabIndex = 14
        Me.btnSave.Text = "Save"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtResult)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 455)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(669, 82)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Result"
        '
        'txtResult
        '
        Me.txtResult.Enabled = False
        Me.txtResult.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResult.Location = New System.Drawing.Point(18, 27)
        Me.txtResult.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtResult.Name = "txtResult"
        Me.txtResult.Size = New System.Drawing.Size(636, 29)
        Me.txtResult.TabIndex = 16
        '
        'CheckBoxSSL
        '
        Me.CheckBoxSSL.AutoSize = True
        Me.CheckBoxSSL.Location = New System.Drawing.Point(370, 18)
        Me.CheckBoxSSL.Name = "CheckBoxSSL"
        Me.CheckBoxSSL.Size = New System.Drawing.Size(144, 24)
        Me.CheckBoxSSL.TabIndex = 16
        Me.CheckBoxSSL.Text = "SSL Required?"
        Me.CheckBoxSSL.UseVisualStyleBackColor = True
        '
        'LabelSMTPPort
        '
        Me.LabelSMTPPort.AutoSize = True
        Me.LabelSMTPPort.Location = New System.Drawing.Point(552, 18)
        Me.LabelSMTPPort.Name = "LabelSMTPPort"
        Me.LabelSMTPPort.Size = New System.Drawing.Size(38, 20)
        Me.LabelSMTPPort.TabIndex = 17
        Me.LabelSMTPPort.Text = "Port"
        '
        'TextBoxPort
        '
        Me.TextBoxPort.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPort.Location = New System.Drawing.Point(556, 45)
        Me.TextBoxPort.Name = "TextBoxPort"
        Me.TextBoxPort.Size = New System.Drawing.Size(83, 36)
        Me.TextBoxPort.TabIndex = 18
        Me.TextBoxPort.Text = "465"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(877, 551)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnDraftOpen)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnDraft)
        Me.Controls.Add(Me.btnSend)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.Text = "IASendEMail"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents EmailMethod_MSGraph As RadioButton
    Friend WithEvents EmailMethod_Office365 As RadioButton
    Friend WithEvents EmailMethod_SMTP As RadioButton
    Friend WithEvents EmailMethod_TPDuet As RadioButton
    Friend WithEvents EmailMethod_EWS As RadioButton
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents CheckBoxSSL As CheckBox
    Friend WithEvents TextBoxPort As TextBox
    Friend WithEvents LabelSMTPPort As Label
End Class
