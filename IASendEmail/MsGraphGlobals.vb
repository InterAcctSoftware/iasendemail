﻿Imports Microsoft.Graph
Imports Microsoft.Identity.Client

Module MsGraphGlobals

    Public graphClient As GraphServiceClient

    'The MSAL Public client app
    Public MSALApplication As IPublicClientApplication

    Public tokenToUse As String

End Module
